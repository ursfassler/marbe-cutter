#pragma once

#include "Shapes.h"
#include "track/Segments.h"


namespace shape
{


shape::Shapes createShapes(
    const track::Segments&,
    double endWidth,
    double heightReduction
    );


}
