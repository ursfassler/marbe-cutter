#pragma once

namespace shape
{


struct MarbleDiameter
{
  double normal{};
  double minimum{};
  double maximum{};
};


}
