#pragma once

namespace track
{

class PillarEntry;
class PillarExit;
class Segment;

}

namespace shape
{


class ShapeBuilder;

void createEndShape(
    const track::PillarEntry&,
    double width,
    double heightReduction,
    shape::ShapeBuilder&
    );
void createStartShape(
    const track::PillarExit&,
    double width,
    double heightReduction,
    shape::ShapeBuilder&
    );
void createSegmentShape(
    const track::Segment&,
    double heightReduction,
    shape::ShapeBuilder&
    );


}
