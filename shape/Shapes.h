#pragma once

#include "geometry/2d/Shape.h"

namespace shape
{


struct Shapes
{
  geometry::_2d::Shape end{};
  std::vector<geometry::_2d::Shape> segments{};
  geometry::_2d::Shape start{};
};


}
