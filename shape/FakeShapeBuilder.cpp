#include "FakeShapeBuilder.h"

namespace shape::test
{


void FakeShapeBuilder::start(const geometry::_2d::Point& value)
{
  toXY(value);
}

void FakeShapeBuilder::toXY(const geometry::_2d::Point& value)
{
  lastPosition = value + offset;
  shape.push_back(lastPosition);
}

void FakeShapeBuilder::moveY(double value)
{
  lastPosition = lastPosition + geometry::_2d::Vector{0, value};
  shape.push_back(lastPosition);
}

void FakeShapeBuilder::toY(double value)
{
  lastPosition = geometry::_2d::Point{lastPosition.x, offset.y+value};
  shape.push_back(lastPosition);
}

void FakeShapeBuilder::moveX(double value)
{
  lastPosition = lastPosition + geometry::_2d::Vector{value, 0};
  shape.push_back(lastPosition);
}

void FakeShapeBuilder::toX(double value)
{
  lastPosition = geometry::_2d::Point{offset.x+value, lastPosition.y};
  shape.push_back(lastPosition);
}

void FakeShapeBuilder::setOffset(const geometry::_2d::Vector& value)
{
  offset = value;
}

void FakeShapeBuilder::slit(const DoubleSlit& s, double position)
{
  slits.push_back({s, {offset.x+position, lastPosition.y}});
}

void FakeShapeBuilder::tab(const Tab& t, double position)
{
  tabs.push_back({t, {offset.x+position, lastPosition.y}});
}


}
