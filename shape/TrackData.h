#pragma once

namespace shape
{


struct TrackData
{
  double gauge{};
  double width{};
};


}
