#include "createShapes.h"
#include "createShape.h"
#include "shape/StraightShapeBuilder.h"

namespace shape
{


shape::Shapes createShapes(
    const track::Segments& segments,
    double endWidth,
    double heightReduction
    )
{
  shape::Shapes result{};

  shape::StraightShapeBuilder end{};
  createEndShape(*segments.front().exitPillar(), endWidth, heightReduction, end);
  result.end = end.getShape();

  for (const auto& segment : segments) {
    shape::StraightShapeBuilder seg{};
    createSegmentShape(segment, heightReduction, seg);
    result.segments.push_back(seg.getShape());
  }

  shape::StraightShapeBuilder start{};
  createStartShape(*segments.back().entryPillar(), endWidth, heightReduction, start);
  result.start = start.getShape();

  return result;
}


}
