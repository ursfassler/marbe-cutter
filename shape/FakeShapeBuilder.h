#pragma once

#include "ShapeBuilder.h"
#include "geometry/2d/Shape.h"

namespace shape::test
{


class FakeShapeBuilder :
    public ShapeBuilder
{
public:
  void start(const geometry::_2d::Point&) override;
  void toXY(const geometry::_2d::Point&) override;
  void moveY(double) override;
  void toY(double) override;
  void moveX(double) override;
  void toX(double) override;
  void setOffset(const geometry::_2d::Vector&) override;
  void slit(const DoubleSlit&, double) override;
  void tab(const Tab&, double) override;

  geometry::_2d::Shape shape{};
  std::vector<std::pair<DoubleSlit, geometry::_2d::Point>> slits{};
  std::vector<std::pair<Tab, geometry::_2d::Point>> tabs{};

private:
  geometry::_2d::Vector offset{};
  geometry::_2d::Point lastPosition{};

};


}
