#include "createShape.h"
#include "shape/ShapeBuilder.h"
#include "track/Segments.h"

namespace shape
{


void createEndShape(
    const track::PillarEntry& endPillar,
    double width,
    double heightReduction,
    shape::ShapeBuilder& builder
    )
{
  builder.start({-width/2, 0});
  builder.toY(endPillar.entryPosition().z - heightReduction);
  builder.toX(width/2);
  builder.toY(0);
}

void createStartShape(
    const track::PillarExit& startPillar,
    double width,
    double heightReduction,
    shape::ShapeBuilder& builder
    )
{
  builder.start({-width/2, 0});
  builder.toY(startPillar.exitPosition().z - heightReduction);
  builder.toX(width/2);
  builder.toY(0);
}


void createSegmentShape(
    const track::Segment& segment,
    double heightReduction,
    shape::ShapeBuilder& builder
    )
{
  builder.start({0, 0});
  builder.toY(segment.exitPillar()->entryPosition().z - heightReduction);
  builder.toXY({segment.length(), segment.entryPillar()->exitPosition().z - heightReduction});
  builder.toY(0);
  builder.toX(0);
}


}
