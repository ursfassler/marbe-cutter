#pragma once

#include "geometry/2d/Point.h"

namespace shape
{


struct DoubleSlit {
  double y{};
  double distance{};
  double width{};
};

struct Tab {
  double width{};
  double height{};
};


class ShapeBuilder
{
public:
  virtual ~ShapeBuilder() = default;

  virtual void start(const geometry::_2d::Point& value) = 0;
  virtual void toXY(const geometry::_2d::Point& value) = 0;
  virtual void moveY(double value) = 0;
  virtual void toY(double value) = 0;
  virtual void moveX(double value) = 0;
  virtual void toX(double value) = 0;
  virtual void setOffset(const geometry::_2d::Vector& value) = 0;

  virtual void slit(const DoubleSlit& s, double position) = 0;
  virtual void tab(const Tab& t, double position) = 0;
};


}
