#include "StraightShapeBuilder.h"

namespace shape
{
namespace
{


enum class SlitDirection
{
  LeftRight,
  RightLeft,
};


}


void StraightShapeBuilder::start(const geometry::_2d::Point& value)
{
  toXY(value);
}

void StraightShapeBuilder::toXY(const geometry::_2d::Point& value)
{
  lastPosition = value + offset;
  shape.push_back(lastPosition);
}

void StraightShapeBuilder::moveY(double value)
{
  lastPosition = lastPosition + geometry::_2d::Vector{0, value};
  shape.push_back(lastPosition);
}

void StraightShapeBuilder::toY(double value)
{
  lastPosition = geometry::_2d::Point{lastPosition.x, offset.y+value};
  shape.push_back(lastPosition);
}

void StraightShapeBuilder::moveX(double value)
{
  lastPosition = lastPosition + geometry::_2d::Vector{value, 0};
  shape.push_back(lastPosition);
}

void StraightShapeBuilder::toX(double value)
{
  lastPosition = geometry::_2d::Point{offset.x+value, lastPosition.y};
  shape.push_back(lastPosition);
}

void StraightShapeBuilder::slit(const DoubleSlit& s, double position)
{
  const auto direction = ((position+offset.x) < shape.back().x) ? SlitDirection::RightLeft : SlitDirection::LeftRight;
  const auto d = direction == SlitDirection::LeftRight ? +1 : -1;

  const auto y = shape.back().y;

  toXY({position-d*s.distance/2-d*s.width, y});
  toXY({position-d*s.distance/2-d*s.width, s.y});
  toXY({position-d*s.distance/2, s.y});
  toXY({position-d*s.distance/2, y});

  toXY({position+d*s.distance/2, y});
  toXY({position+d*s.distance/2, s.y});
  toXY({position+d*s.distance/2+d*s.width, s.y});
  toXY({position+d*s.distance/2+d*s.width, y});
}

void StraightShapeBuilder::tab(const Tab& t, double position)
{
  const auto direction = ((position+offset.x) < shape.back().x) ? SlitDirection::RightLeft : SlitDirection::LeftRight;
  const auto d = direction == SlitDirection::LeftRight ? +1 : -1;

  toXY({position-d*t.width/2, 0.0});
  toXY({position-d*t.width/2, -t.height});
  toXY({position+d*t.width/2, -t.height});
  toXY({position+d*t.width/2, 0.0});
}

const geometry::_2d::Shape& StraightShapeBuilder::getShape() const
{
  return shape;
}

void StraightShapeBuilder::setOffset(const geometry::_2d::Vector& value)
{
  offset = value;
}


}
