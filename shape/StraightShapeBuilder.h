#pragma once

#include "ShapeBuilder.h"
#include "geometry/2d/Shape.h"

namespace shape
{


class StraightShapeBuilder :
    public ShapeBuilder
{
public:
  void start(const geometry::_2d::Point& value) override;
  void toXY(const geometry::_2d::Point& value) override;
  void moveY(double value) override;
  void toY(double value) override;
  void moveX(double value) override;
  void toX(double value) override;
  void setOffset(const geometry::_2d::Vector& value) override;

  void slit(const DoubleSlit& s, double position) override;
  void tab(const Tab& t, double position) override;

  const geometry::_2d::Shape& getShape() const;

private:
  geometry::_2d::Vector offset{};
  geometry::_2d::Point lastPosition{};
  geometry::_2d::Shape shape{};

};


}
