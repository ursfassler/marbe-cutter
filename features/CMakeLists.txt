add_executable(feature-tests
  "produce output.feature"
  step_definitions/steps.cpp
)

include_directories(feature-tests
  .
  ../
)

target_link_libraries(feature-tests
  cucumber-cpp
  boost_regex boost_system boost_program_options boost_filesystem
  pthread
  gtest
  GLU
  mc
)
