#include "cutter/cutter.h"
#include "model/Model.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace features
{
namespace
{


struct Context
{
  model::SegmentLengths segments{};
  std::optional<model::Model> model{};
  std::string output{};
};


GIVEN("^I have the segments:$")
{
  TABLE_PARAM(segmentTable);

  cucumber::ScenarioScope<Context> context{};
  context->segments.clear();

  for (const auto& entry : segmentTable.hashes()) {
    const double length = std::stod(entry.at("length"));
    context->segments.push_back(length);
  }
}

WHEN("^I load the \"drop\" model$")
{
  cucumber::ScenarioScope<Context> context{};
  context->model.emplace(cutter::createDropModel(context->segments));
}

WHEN("^generate the path$")
{
  cucumber::ScenarioScope<Context> context{};

  std::stringstream ss{};
  cutter::writePath(ss, *context->model);

  context->output = ss.str();
}

WHEN("^generate the side segments$")
{
  cucumber::ScenarioScope<Context> context{};

  std::stringstream ss{};
  cutter::writeSideSegments(ss, *context->model);

  context->output = ss.str();
}

WHEN("^generate the x3d model$")
{
  cucumber::ScenarioScope<Context> context{};

  std::stringstream ss{};
  cutter::writeX3dModel(ss, *context->model);

  context->output = ss.str();
}

THEN("^I expect the output:$")
{
  REGEX_PARAM(std::string, expected);
  cucumber::ScenarioScope<Context> context{};

  ASSERT_EQ(expected, context->output);
}


}
}
