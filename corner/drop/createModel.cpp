#include "createModel.h"
#include "DropPillars.h"
#include "createShapes.h"

namespace corner::drop
{


model::Model createModel(
    const model::SegmentLengths& segmentLengths,
    const track::HeightParameter& heightParameter,
    const shape::TrackData& trackData,
    const shape::TabData& tabData,
    const shape::MarbleDiameter& marbleDiameter,
    const CornerSpacing& cornerSpacing
    )
{
  track::Pillars pillars{createPillars(segmentLengths, heightParameter, trackData.gauge, cornerSpacing)};
  track::Segments segments{createSegments(pillars)};
  shape::Shapes shapes{createShapes(segments, trackData, tabData, marbleDiameter, cornerSpacing)};

  return {trackData, tabData, marbleDiameter, pillars, segments, shapes};
}


}
