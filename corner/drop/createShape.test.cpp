#include "createShape.h"
#include "DropPillars.h"
#include "geometry/2d/toStream.h"
#include "shape/FakeShapeBuilder.h"
#include "shape/MarbleDiameter.h"
#include "shape/ShapeBuilder.h"
#include "shape/TabData.h"
#include "shape/TrackData.h"
#include "track/Segments.h"
#include "track/testHelper.h"
#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

namespace corner::drop::test
{

using track::test::EqP2;


class CreateShapes :
    public ::testing::Test
{
protected:
  const std::vector<double> segmentLengths
  {
    60,
    40,
    -90,
  };
  const track::HeightParameter heightParameter
  {
    .slope = 0.05,
    .endHeight = 15,
  };
  const CornerSpacing cornerSpacing
  {

    .width = 5,
    .height = 5,
  };
  const shape::MarbleDiameter diameter
  {
    .normal = 16,
    .minimum = 15,
    .maximum = 17,
  };
  const shape::TrackData trackData
  {
    .gauge = 10,
    .width = 0.5,
  };
  const shape::TabData tabData
  {
    .length = 10,
    .deepthReduction = 0.2,
  };
  const double eps{0.0001};
};

TEST_F(CreateShapes, create_side_shape)
{
  const track::Pillars pillars{createPillars(segmentLengths, heightParameter, trackData.gauge, cornerSpacing)};
  const track::Segments segments{createSegments(pillars)};

  shape::test::FakeShapeBuilder builder{};
  ASSERT_EQ(segments.size(), 3);
  createSegmentShape(segments.at(1), trackData, tabData, diameter, cornerSpacing, builder);

  const auto shape = builder.shape;
  ASSERT_EQ(shape.size(), 11);

  EXPECT_THAT(shape.at(0),  EqP2({ 10,  0 }));
  EXPECT_THAT(shape.at(1),  EqP2({-15,  0 }));
  EXPECT_THAT(shape.at(2),  EqP2({-15, 24.2550 }));
  EXPECT_THAT(shape.at(3),  EqP2({-10, 24.2550 }));
  EXPECT_THAT(shape.at(4),  EqP2({-10, 12.7550 }));
  EXPECT_THAT(shape.at(5),  EqP2({ 10, 12.7550 }));
  EXPECT_THAT(shape.at(6),  EqP2({ 10, 24.2550 }));
  EXPECT_THAT(shape.at(7),  EqP2({ 30, 25.2550 }));
  EXPECT_THAT(shape.at(8),  EqP2({ 50, 26.2550 }));
  EXPECT_THAT(shape.at(9),  EqP2({ 50,  0 }));
  EXPECT_THAT(shape.at(10), EqP2({ 30,  0 }));

  const auto slits = builder.slits;
  ASSERT_EQ(slits.size(), 2);

  EXPECT_NEAR(slits.at(0).first.y, 6.3775, eps);
  EXPECT_NEAR(slits.at(0).first.distance, 10, eps);
  EXPECT_NEAR(slits.at(0).first.width, 0.5, eps);
  EXPECT_THAT(slits.at(0).second, EqP2({0, 12.7550}));

  EXPECT_NEAR(slits.at(1).first.y, 10.3775, eps);
  EXPECT_NEAR(slits.at(1).first.distance, 10, eps);
  EXPECT_NEAR(slits.at(1).first.width, 0.5, eps);
  EXPECT_THAT(slits.at(1).second, EqP2({40, 0}));

  const auto tabs = builder.tabs;
  ASSERT_EQ(tabs.size(), 1);

  EXPECT_NEAR(tabs.at(0).first.width, 10, eps);
  EXPECT_NEAR(tabs.at(0).first.height, 0.4, eps);
  EXPECT_THAT(tabs.at(0).second, EqP2({0, 0}));
}

TEST_F(CreateShapes, create_start_shape)
{
  const track::Pillars pillars{createPillars(segmentLengths, heightParameter, trackData.gauge, cornerSpacing)};

  shape::test::FakeShapeBuilder builder{};
  createStartShape(*pillars.start, trackData, tabData, diameter, builder);

  const auto shape = builder.shape;
  ASSERT_EQ(shape.size(), 4);

  EXPECT_THAT(shape.at(0), EqP2({-10, 31.255}));
  EXPECT_THAT(shape.at(1), EqP2({ 10, 31.2550}));
  EXPECT_THAT(shape.at(2), EqP2({ 10,  0 }));
  EXPECT_THAT(shape.at(3), EqP2({-10,  0 }));

  const auto slits = builder.slits;
  ASSERT_EQ(slits.size(), 1);

  EXPECT_NEAR(slits.at(0).first.y, 15.6275, eps);
  EXPECT_NEAR(slits.at(0).first.distance, 10, eps);
  EXPECT_NEAR(slits.at(0).first.width, 0.5, eps);
  EXPECT_THAT(slits.at(0).second, EqP2({0, 31.255}));

  const auto tabs = builder.tabs;
  ASSERT_EQ(tabs.size(), 1);

  EXPECT_NEAR(tabs.at(0).first.width, 10, eps);
  EXPECT_NEAR(tabs.at(0).first.height, 0.4, eps);
  EXPECT_THAT(tabs.at(0).second, EqP2({0, 0}));
}

TEST_F(CreateShapes, create_end_shape)
{
  const track::Pillars pillars{createPillars(segmentLengths, heightParameter, trackData.gauge, cornerSpacing)};

  shape::test::FakeShapeBuilder builder{};
  createEndShape(*pillars.end, trackData, diameter, builder);

  const auto shape = builder.shape;
  ASSERT_EQ(shape.size(), 8);

  EXPECT_THAT(shape.at(0), EqP2({-15, 15.2550}));
  EXPECT_THAT(shape.at(1), EqP2({-10, 15.2550}));
  EXPECT_THAT(shape.at(2), EqP2({-10,  8.7550}));
  EXPECT_THAT(shape.at(3), EqP2({ 10,  8.7550}));
  EXPECT_THAT(shape.at(4), EqP2({ 10, 15.2550}));
  EXPECT_THAT(shape.at(5), EqP2({ 15, 15.2550}));
  EXPECT_THAT(shape.at(6), EqP2({ 15,  0}));
  EXPECT_THAT(shape.at(7), EqP2({-15,  0}));

  const auto slits = builder.slits;
  ASSERT_EQ(slits.size(), 1);

  EXPECT_NEAR(slits.at(0).first.y, 4.3775, eps);
  EXPECT_NEAR(slits.at(0).first.distance, 10, eps);
  EXPECT_NEAR(slits.at(0).first.width, 0.5, eps);
  EXPECT_THAT(slits.at(0).second, EqP2({0, 0}));

  const auto tabs = builder.tabs;
  ASSERT_EQ(tabs.size(), 0);
}

TEST_F(CreateShapes, configuration_is_sane)
{
  ASSERT_TRUE(isSane(diameter, heightParameter.slope, trackData, cornerSpacing));
}


}
