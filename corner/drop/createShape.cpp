#include "createShape.h"
#include "DropPillars.h"
#include "shape/MarbleDiameter.h"
#include "shape/ShapeBuilder.h"
#include "shape/TabData.h"
#include "shape/TrackData.h"
#include "track/Segments.h"
#include <cmath>

namespace corner::drop
{
namespace
{


constexpr double stopperWidth = 5;
constexpr double interlockExtension = 5;

double tabHeight(const shape::TrackData& trackData, const shape::TabData& tabData)
{
  return trackData.width * (1-tabData.deepthReduction);
}

double sqr(double x)
{
  return x * x;
}

double normalAxleHeight(const shape::TrackData& trackData, const shape::MarbleDiameter& marbleDiameter)
{
  return std::sqrt(sqr(marbleDiameter.normal/2) - sqr(trackData.gauge/2));
}

double endCut(const shape::TrackData& trackData, double cornerWidthSpacing)
{
  return trackData.gauge/2 + cornerWidthSpacing;
}

double minimalCornerHeightSpacing(const shape::MarbleDiameter& diameter, double slope, const shape::TrackData& trackData)
{
  const auto marbleCenterAboveTrack = std::sqrt(sqr(diameter.minimum/2) + sqr(trackData.gauge/2));
  const auto marbleBelowTrack = diameter.minimum/2 - marbleCenterAboveTrack;

  const auto trackHeightGain = (trackData.width + trackData.gauge/2) * slope;
  return marbleBelowTrack + trackHeightGain;
}

double minimalCornerWidthSpacing(const shape::MarbleDiameter& diameter, const shape::TrackData& trackData)
{
  return diameter.maximum/2 - trackData.gauge/2;
}


}


void createEndShape(
    const track::PillarEntry& endPillar,
    const shape::TrackData& trackData,
    const shape::MarbleDiameter& marbleDiameter,
    shape::ShapeBuilder& builder
    )
{
  const auto axleHeight = normalAxleHeight(trackData, marbleDiameter);
  const auto exitHeight = endPillar.entryPosition().z - axleHeight;

  const auto interlockHeight = endPillar.interlockHeight(axleHeight);
  const auto interlockWidth = interlockExtension + trackData.gauge + interlockExtension;
  shape::DoubleSlit slit{};
  slit.y = interlockHeight / 2;
  slit.distance = trackData.gauge;
  slit.width = trackData.width;

  builder.setOffset({-interlockWidth/2, 0});

  builder.start({-stopperWidth, exitHeight});
  builder.toX(0);
  builder.toY(interlockHeight);
  builder.toX(interlockWidth);
  builder.toY(exitHeight);
  builder.moveX(stopperWidth);
  builder.toY(0);
  builder.slit(slit, interlockWidth/2);
  builder.toX(-stopperWidth);
}

void createStartShape(
    const track::PillarExit& startPillar,
    const shape::TrackData& trackData,
    const shape::TabData& tabData,
    const shape::MarbleDiameter& marbleDiameter,
    shape::ShapeBuilder& builder
    )
{
  const auto axleHeight = normalAxleHeight(trackData, marbleDiameter);

  const auto interlockHeight = startPillar.interlockHeight(axleHeight);
  const auto interlockWidth = interlockExtension + trackData.gauge + interlockExtension;
  shape::DoubleSlit slit{};
  slit.y = interlockHeight / 2;
  slit.distance = trackData.gauge;
  slit.width = trackData.width;
  shape::Tab tab{};
  tab.height = tabHeight(trackData, tabData);
  tab.width = tabData.length;

  builder.setOffset({-interlockWidth/2, 0});

  builder.start({0, interlockHeight});
  builder.slit(slit, interlockWidth/2);
  builder.toX(interlockWidth);
  builder.toY(0);
  builder.tab(tab, interlockWidth/2);
  builder.toX(0);
}


void createSegmentShape(
    const track::Segment& segment,
    const shape::TrackData& trackData,
    const shape::TabData& tabData,
    const shape::MarbleDiameter& marbleDiameter,
    const CornerSpacing& cornerSpacing,
    shape::ShapeBuilder& builder
    )
{
  const auto axleHeight = normalAxleHeight(trackData, marbleDiameter);
  const auto xZero = segment.exitPillar()->entryOffset().xyLength;

  builder.setOffset({xZero, 0});

  {
    const auto exitEntryHeight = segment.exitPillar()->entryPosition().z - axleHeight;
    const auto exitEntryDistance = xZero;

    const auto exitInterlockHeight = segment.exitPillar()->interlockHeight(axleHeight);
    const auto exitInterlockWidth = endCut(trackData, cornerSpacing.width) + trackData.gauge/2 + interlockExtension;

    const auto exitStopperLength = stopperWidth;
    const auto exitStopperHeight = exitEntryHeight;

    shape::DoubleSlit exitInterlockSlit{};
    exitInterlockSlit.y = exitInterlockHeight / 2;
    exitInterlockSlit.distance = trackData.gauge;
    exitInterlockSlit.width = trackData.width;

    shape::Tab exitTab{};
    exitTab.height = tabHeight(trackData, tabData);
    exitTab.width = tabData.length;

    builder.start({0, 0});
    builder.tab(exitTab, -exitEntryDistance);
    builder.toX(-exitStopperLength-exitInterlockWidth);
    builder.toY(exitStopperHeight);
    builder.toX(-exitInterlockWidth);
    builder.toY(exitInterlockHeight);
    builder.slit(exitInterlockSlit, -exitEntryDistance);
    builder.toX(0);
    builder.toY(exitEntryHeight);
  }

  builder.setOffset({xZero+segment.length(), 0});

  {
    const auto entryExtension = trackData.gauge/2;
    const auto entryHeight = segment.entryPillar()->exitPosition().z - axleHeight;
    const auto entryWidth = endCut(trackData, cornerSpacing.width) + entryExtension + interlockExtension;

    const auto entryExitHeight = entryHeight;
    const auto entryExitDistance = segment.entryPillar()->exitOffset().xyLength;
    const auto entryEntryHeight = entryHeight + (entryWidth * segment.slope());
    const auto entryLength = interlockExtension + trackData.gauge + interlockExtension;

    shape::DoubleSlit entryTrackSlit{};
    entryTrackSlit.y = segment.entryPillar()->interlockHeight(axleHeight) / 2;
    entryTrackSlit.distance = trackData.gauge;
    entryTrackSlit.width = trackData.width;

    builder.toXY({0, entryExitHeight });
    builder.toXY({entryLength, entryEntryHeight});
    builder.toY(0);
    builder.slit(entryTrackSlit, entryExitDistance);
    builder.toX(0);
  }
}

bool isSane(
    const shape::MarbleDiameter& diameter,
    double slope,
    const shape::TrackData& trackData,
    const CornerSpacing& cornerSpacing
    )
{
  const auto maximalTrackGauge = diameter.minimum - diameter.minimum*0.1;
  const auto minimalTrackGauge = diameter.maximum*0.5;
  if (trackData.gauge > maximalTrackGauge) {
    return false;
  }
  if (trackData.gauge < minimalTrackGauge) {
    return false;
  }

  if (cornerSpacing.height < minimalCornerHeightSpacing(diameter, slope, trackData)) {
    return false;
  }

  if (cornerSpacing.width < minimalCornerWidthSpacing(diameter, trackData)) {
    return false;
  }

  return true;
}


}
