#pragma once

#include "model/Model.h"

namespace track
{

struct HeightParameter;

}

namespace corner::drop
{


struct CornerSpacing;

model::Model createModel(
    const model::SegmentLengths&,
    const track::HeightParameter&,
    const shape::TrackData&,
    const shape::TabData&,
    const shape::MarbleDiameter&,
    const CornerSpacing&
    );


}
