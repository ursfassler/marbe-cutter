#pragma once

#include "geometry/2d/Point.h"
#include "geometry/3d/Point.h"
#include "geometry/3d/RotVec.h"
#include "track/HeightParameter.h"
#include "track/Pillars.h"
#include <memory>
#include <vector>

namespace corner::drop
{


class EdgePillar :
    public track::PillarCorner
{
public:
  EdgePillar(
      const geometry::_2d::Point&,
      double height,
      const geometry::_3d::RotVec& entryOffset,
      const geometry::_3d::RotVec& exitOffset,
      double cornerHeightSpacing
      );

  geometry::_2d::Point position() const override;
  geometry::_3d::Point entryPosition() const override;
  geometry::_3d::Point exitPosition() const override;
  geometry::_3d::RotVec entryOffset() const override;
  geometry::_3d::RotVec exitOffset() const override;
  double interlockHeight(double axleHeight) const override;

  std::vector<geometry::_3d::Point> path() const override;
  std::optional<track::Tab> tab() const override;

private:
  const geometry::_2d::Point position_;
  const double height_;
  const geometry::_3d::RotVec entryOffset_;
  const geometry::_3d::RotVec exitOffset_;
  double cornerHeightSpacing_;
};

class EndPillar :
    public track::PillarEntry
{
public:
  EndPillar(
      const geometry::_2d::Point&,
      double height,
      const geometry::_3d::RotVec& entryOffset
      );

  geometry::_2d::Point position() const override;
  geometry::_3d::Point entryPosition() const override;
  geometry::_3d::RotVec entryOffset() const override;
  double interlockHeight(double axleHeight) const override;

  std::vector<geometry::_3d::Point> path() const override;
  std::optional<track::Tab> tab() const override;

private:
  const geometry::_2d::Point position_;
  const double height_;
  const geometry::_3d::RotVec entryOffset_;

};

class StartPillar :
    public track::PillarExit
{
public:
  StartPillar(
      const geometry::_2d::Point&,
      double height,
      const geometry::_3d::RotVec& exitOffset,
      double cornerHeightSpacing
      );

  geometry::_2d::Point position() const override;
  geometry::_3d::Point exitPosition() const override;
  geometry::_3d::RotVec exitOffset() const override;
  double interlockHeight(double axleHeight) const override;

  std::vector<geometry::_3d::Point> path() const override;
  std::optional<track::Tab> tab() const override;

private:
  const geometry::_2d::Point position_;
  const double height_;
  const geometry::_3d::RotVec exitOffset_;
  double cornerHeightSpacing_;
};

struct CornerSpacing
{
  double width{};
  double height{};
};

track::Pillars createPillars(const std::vector<double>& segments, const track::HeightParameter&, double trackGauge, const CornerSpacing&);


}
