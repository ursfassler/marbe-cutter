#include "DropPillars.h"
#include "geometry/2d/rotate.h"
#include "geometry/projection.h"
#include <cmath>

namespace corner::drop
{
namespace
{


constexpr double CornerFallHeight = 6;

double calcEntryHeight(double exitHeight)
{
  return exitHeight + CornerFallHeight;
}

geometry::_3d::RotVec entry(std::size_t i, const std::vector<geometry::_2d::Direction>& entryDirections, double distance, double deltaHeight)
{
  return {
    .direction = entryDirections.at(i),
    .xyLength = distance,
    .z = deltaHeight,
  };
}

geometry::_3d::RotVec exit(std::size_t i, const std::vector<geometry::_2d::Direction>& entryDirections, double distance, double deltaHeight)
{
  return {
    .direction = rotateQuarter(rotateQuarter(entryDirections.at(i-1))),
    .xyLength = distance,
    .z = -deltaHeight,
  };
}


}

track::Pillars createPillars(const std::vector<double>& segments, const track::HeightParameter& heightParameter, double trackGauge, const CornerSpacing& cornerSpacing)
{
  std::vector<geometry::_2d::Point> positions{};
  std::vector<geometry::_2d::Direction> entryDirections{};
  std::vector<double> heights{};

  geometry::_2d::Point pos{0, 0};

  positions.push_back(pos);
  heights.push_back(heightParameter.endHeight);
  for(std::size_t i = 0; i < segments.size(); i++) {
    const auto length = segments[i];
    const auto horizontal = (i % 2) == 0;
    const auto positive = length > 0;
    const auto direction = geometry::_2d::getDirection(horizontal, positive);

    pos = pos + geometry::_2d::rotateTo(std::abs(length), direction);

    const auto deltaHeight = std::abs(length) * heightParameter.slope;
    const auto nextExitHeight = calcEntryHeight(heights.back()) + deltaHeight;

    entryDirections.push_back(direction);
    positions.push_back(pos);

    heights.push_back(nextExitHeight);
  }

  const double distance = trackGauge/2 + cornerSpacing.width;
  const double deltaHeight = distance * heightParameter.slope;

  track::Pillars result{};

  {
    const auto i = 0;
    const auto entryOffset = entry(i, entryDirections, distance, deltaHeight);
    result.end = std::make_shared<EndPillar>(positions.at(i), heights.at(i), entryOffset);
  }

  for (std::size_t i = 1; i < positions.size()-1; i++) {
    const auto entryOffset = entry(i, entryDirections, distance, deltaHeight);
    const auto exitOffset = exit(i, entryDirections, distance, deltaHeight);
    result.edges.push_back(std::make_shared<EdgePillar>(positions.at(i), heights.at(i), entryOffset, exitOffset, cornerSpacing.height));
  }

  {
    const auto i = positions.size()-1;
    const auto exitOffset = exit(i, entryDirections, distance, deltaHeight);
    result.start = std::make_shared<StartPillar>(positions.at(i), heights.at(i), exitOffset, cornerSpacing.height);
  }

  return result;
}


EdgePillar::EdgePillar(
    const geometry::_2d::Point& position,
    double height,
    const geometry::_3d::RotVec& entryOffset,
    const geometry::_3d::RotVec& exitOffset,
    double cornerHeightSpacing
    ) :
  position_{position},
  height_{height},
  entryOffset_{entryOffset},
  exitOffset_{exitOffset},
  cornerHeightSpacing_{cornerHeightSpacing}
{
}

geometry::_2d::Point EdgePillar::position() const
{
  return position_;
}

geometry::_3d::Point EdgePillar::entryPosition() const
{
  return geometry::as3d(position_, calcEntryHeight(height_)) + entryOffset_;
}

geometry::_3d::Point EdgePillar::exitPosition() const
{
  return geometry::as3d(position_, height_) + exitOffset_;
}

geometry::_3d::RotVec EdgePillar::entryOffset() const
{
  return entryOffset_;
}

geometry::_3d::RotVec EdgePillar::exitOffset() const
{
  return exitOffset_;
}

double EdgePillar::interlockHeight(double axleHeight) const
{
  return height_ - cornerHeightSpacing_ - axleHeight;
}

std::vector<geometry::_3d::Point> EdgePillar::path() const
{
  return {
    entryPosition(),
    geometry::as3d(position(), height_),
    exitPosition(),
  };
}

std::optional<track::Tab> EdgePillar::tab() const
{
  return {{ position_, entryOffset_.direction }};
}



EndPillar::EndPillar(
    const geometry::_2d::Point& position,
    double height,
    const geometry::_3d::RotVec& entryOffset
    ) :
  position_{position},
  height_{height},
  entryOffset_{entryOffset}
{
}

geometry::_2d::Point EndPillar::position() const
{
  return position_;
}

geometry::_3d::Point EndPillar::entryPosition() const
{
  return geometry::as3d(position_, calcEntryHeight(height_)) + entryOffset_;
}

geometry::_3d::RotVec EndPillar::entryOffset() const
{
  return entryOffset_;
}

double EndPillar::interlockHeight(double axleHeight) const
{
  return height_ - axleHeight;
}

std::vector<geometry::_3d::Point> EndPillar::path() const
{
  return {
    entryPosition(),
    geometry::as3d(position(), height_),
  };
}

std::optional<track::Tab> EndPillar::tab() const
{
  return {{ position_, entryOffset_.direction }};
}



StartPillar::StartPillar(
    const geometry::_2d::Point& position,
    double height,
    const geometry::_3d::RotVec& exitOffset,
    double cornerHeightSpacing
    ) :
  position_{position},
  height_{height},
  exitOffset_{exitOffset},
  cornerHeightSpacing_{cornerHeightSpacing}
{
}

geometry::_2d::Point StartPillar::position() const
{
  return position_;
}

geometry::_3d::Point StartPillar::exitPosition() const
{
  return geometry::as3d(position_, height_) + exitOffset_;
}

geometry::_3d::RotVec StartPillar::exitOffset() const
{
  return exitOffset_;
}

double StartPillar::interlockHeight(double axleHeight) const
{
  return height_ - cornerHeightSpacing_ - axleHeight;
}

std::vector<geometry::_3d::Point> StartPillar::path() const
{
  return {
    geometry::as3d(position(), height_),
    exitPosition(),
  };
}

std::optional<track::Tab> StartPillar::tab() const
{
  return {{ position_, geometry::_2d::rotateQuarter(exitOffset_.direction) }};
}


}
