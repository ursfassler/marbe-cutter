#include "DropPillars.h"
#include "geometry/2d/toStream.h"
#include "geometry/3d/toStream.h"
#include "track/testHelper.h"
#include <gtest/gtest.h>

namespace corner::drop::test
{

using track::test::EqP2;
using track::test::EqP3;


class PillarsTest :
    public ::testing::Test
{
protected:
  const track::HeightParameter heightParameter
  {
    .slope = 0.05,
    .endHeight = 15,
  };
  const corner::drop::CornerSpacing cornerSpacing
  {

    .width = 5,
    .height = 5,
  };
  const track::Pillars testee{createPillars({
    60,
    40,
    -90,
  }, heightParameter, 10, cornerSpacing)};
  const double eps{0.0001};
};


TEST_F(PillarsTest, all_properties)
{
  ASSERT_EQ(testee.edges.size(), 2);

  const auto p0 = testee.end;
  EXPECT_THAT(p0->position(), EqP2({0, 0}));
  EXPECT_NEAR(p0->interlockHeight(5), 10, eps);
  EXPECT_EQ(p0->entryOffset().direction,  geometry::_2d::Direction::East);
  EXPECT_NEAR(p0->entryOffset().xyLength, 10, eps);
  EXPECT_THAT(p0->entryPosition(), EqP3({10, 0, 21.5}));

  const auto p1 = testee.edges.at(0);
  EXPECT_THAT(p1->position(), EqP2({60, 0}));
  EXPECT_NEAR(p1->interlockHeight(5), 14, eps);
  EXPECT_EQ(p1->exitOffset().direction,  geometry::_2d::Direction::West);
  EXPECT_NEAR(p1->exitOffset().xyLength, 10, eps);
  EXPECT_THAT(p1->exitPosition(), EqP3({50, 0, 23.5}));
  EXPECT_EQ(p1->entryOffset().direction,  geometry::_2d::Direction::North);
  EXPECT_NEAR(p1->entryOffset().xyLength, 10, eps);
  EXPECT_THAT(p1->entryPosition(), EqP3({60, 10, 30.5}));

  const auto p2 = testee.edges.at(1);
  EXPECT_THAT(p2->position(), EqP2({60, 40}));
  EXPECT_NEAR(p2->interlockHeight(5), 22, eps);
  EXPECT_EQ(p2->exitOffset().direction,  geometry::_2d::Direction::South);
  EXPECT_NEAR(p2->exitOffset().xyLength, 10, eps);
  EXPECT_THAT(p2->exitPosition(), EqP3({60, 30, 31.5}));
  EXPECT_EQ(p2->entryOffset().direction,  geometry::_2d::Direction::West);
  EXPECT_NEAR(p2->entryOffset().xyLength, 10, eps);
  EXPECT_THAT(p2->entryPosition(), EqP3({50, 40, 38.5}));

  const auto p3 = testee.start;
  EXPECT_THAT(p3->position(), EqP2({-30, 40}));
  EXPECT_NEAR(p3->interlockHeight(5), 32.5, eps);
  EXPECT_EQ(p3->exitOffset().direction,  geometry::_2d::Direction::East);
  EXPECT_NEAR(p3->exitOffset().xyLength, 10, eps);
  EXPECT_THAT(p3->exitPosition(), EqP3({-20, 40, 42}));
}


}
