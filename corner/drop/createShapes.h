#pragma once

#include "shape/Shapes.h"
#include "track/Segments.h"


namespace shape
{

struct TabData;
struct TrackData;
struct MarbleDiameter;

}

namespace corner::drop
{

struct CornerSpacing;


shape::Shapes createShapes(
    const track::Segments&,
    const shape::TrackData&,
    const shape::TabData&,
    const shape::MarbleDiameter&,
    const CornerSpacing&
    );


}
