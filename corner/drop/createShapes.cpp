#include "createShapes.h"
#include "createShape.h"
#include "shape/StraightShapeBuilder.h"

namespace corner::drop
{


shape::Shapes createShapes(
    const track::Segments& segments,
    const shape::TrackData& trackData,
    const shape::TabData& tabData,
    const shape::MarbleDiameter& diameter,
    const CornerSpacing& cornerSpacing
    )
{
  shape::Shapes result{};

  shape::StraightShapeBuilder end{};
  createEndShape(*segments.front().exitPillar(), trackData, diameter, end);
  result.end = end.getShape();

  for (const auto& segment : segments) {
    shape::StraightShapeBuilder seg{};
    createSegmentShape(segment, trackData, tabData, diameter, cornerSpacing, seg);
    result.segments.push_back(seg.getShape());
  }

  shape::StraightShapeBuilder start{};
  createStartShape(*segments.back().entryPillar(), trackData, tabData, diameter, start);
  result.start = start.getShape();

  return result;
}


}
