#pragma once

namespace track
{

class PillarEntry;
class PillarExit;
class Segment;

}

namespace shape
{

class ShapeBuilder;
struct TrackData;
struct MarbleDiameter;
struct TabData;

}

namespace corner::drop
{


struct CornerSpacing;

void createEndShape(
    const track::PillarEntry&,
    const shape::TrackData&,
    const shape::MarbleDiameter&,
    shape::ShapeBuilder&
    );
void createStartShape(
    const track::PillarExit&,
    const shape::TrackData&,
    const shape::TabData&,
    const shape::MarbleDiameter&,
    shape::ShapeBuilder&
    );
void createSegmentShape(
    const track::Segment&,
    const shape::TrackData&,
    const shape::TabData&,
    const shape::MarbleDiameter&,
    const CornerSpacing&,
    shape::ShapeBuilder&
    );

bool isSane(
    const shape::MarbleDiameter&,
    double slope,
    const shape::TrackData&,
    const CornerSpacing&
    );


}
