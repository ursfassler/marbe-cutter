#include "ShapeWithHoles.h"

namespace geometry::_2d
{


ShapeWithHoles moveBy(const ShapeWithHoles& value, const Vector& delta)
{
  ShapeWithHoles result{};
  result.outline = moveBy(value.outline, delta);
  for (const auto& hole : value.holes) {
    result.holes.push_back(moveBy(hole, delta));
  }
  return result;
}


}
