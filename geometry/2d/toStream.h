#pragma once

#include "Point.h"
#include <ostream>

namespace geometry::_2d
{


std::ostream& operator<<(std::ostream&, const Point&);
std::ostream& operator<<(std::ostream&, const Vector&);


}
