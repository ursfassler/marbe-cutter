#pragma once

#include "Shape.h"
#include <vector>

namespace geometry::_2d
{


struct ShapeWithHoles {
  Shape outline{};
  std::vector<Shape> holes{};
};


ShapeWithHoles moveBy(const ShapeWithHoles&, const Vector&);


}
