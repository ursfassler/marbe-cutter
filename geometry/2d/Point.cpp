#include "Point.h"
#include <algorithm>
#include <cmath>

namespace geometry::_2d
{


Vector operator-(const Vector& value)
{
  return {
    .x = -value.x,
    .y = -value.y,
  };
}

Point operator+(const Point& left, const Vector& right)
{
  return {
    .x = left.x + right.x,
    .y = left.y + right.y,
  };
}

Vector operator-(const Point& left, const Point& right)
{
  return {
    .x = left.x - right.x,
    .y = left.y - right.y,
  };
}

Point min(const Point& left, const Point& right)
{
  return {
    .x = std::min(left.x, right.x),
    .y = std::min(left.y, right.y),
  };
}

Point max(const Point& left, const Point& right)
{
  return {
    .x = std::max(left.x, right.x),
    .y = std::max(left.y, right.y),
  };
}

Vector operator/(const Vector& left, double right)
{
  return {
    .x = left.x / right,
    .y = left.y / right,
  };
}

double length(const Vector& v)
{
  return std::sqrt(v.x*v.x + v.y*v.y);
}


}
