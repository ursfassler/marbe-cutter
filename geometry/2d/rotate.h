#pragma once

#include "Direction.h"
#include "Point.h"
#include <cstdint>

namespace geometry::_2d
{


Vector rotate(const Vector&, Direction);
Vector rotateTo(double length, Direction);
std::size_t rotateCount(Direction);
Direction getDirection(bool horizontal, bool positive);
Direction rotateQuarter(Direction);
Direction getDirection(const Point&, const Point&);


}
