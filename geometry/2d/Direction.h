#pragma once

namespace geometry::_2d
{


enum class Direction {
  East,
  North,
  West,
  South,
};


}
