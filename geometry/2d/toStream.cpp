#include "toStream.h"

namespace geometry::_2d
{


std::ostream& operator<<(std::ostream& os, const Point& value)
{
  return os << "(" << value.x << ", " << value.y << ")";
}

std::ostream& operator<<(std::ostream& os, const Vector& value)
{
  return os << "(" << value.x << ", " << value.y << ")";
}


}
