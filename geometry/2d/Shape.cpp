#include "Shape.h"

namespace geometry::_2d
{


Shape moveBy(const Shape& value, const Vector& delta)
{
  Shape result{};
  for (const auto& point : value) {
    result.emplace_back(point + delta);
  }
  return result;
}

Point min(const Shape& value)
{
  Point result{0, 0};

  for (const auto& point : value) {
    result = min(result, point);
  }

  return result;
}

Point max(const Shape& value)
{
  Point result{0, 0};

  for (const auto& point : value) {
    result = max(result, point);
  }

  return result;
}


}
