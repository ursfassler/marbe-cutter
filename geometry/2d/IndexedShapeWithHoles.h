#pragma once

#include "Point.h"
#include <array>
#include <vector>

namespace geometry::_2d
{


using TriangleIndices = std::array<std::size_t,3>;
using TrianglesIndices = std::vector<TriangleIndices>;

struct IndexedShapeWithHoles
{
  std::vector<Point> points{};
  TrianglesIndices indices{};
  std::vector<std::size_t> outline{};
  std::vector<std::vector<std::size_t>> holes{};
};


}
