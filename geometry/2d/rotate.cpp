#include "rotate.h"
#include <cmath>
#include <stdexcept>

namespace geometry::_2d
{


Vector rotate(const Vector& point, Direction direction)
{
  switch (direction) {
    case Direction::East:
      return {
        .x = point.x,
        .y = point.y,
      };
    case Direction::West:
      return {
        .x = -point.x,
        .y = -point.y,
      };
    case Direction::North:
      return {
        .x = -point.y,
        .y = point.x,
      };
    case Direction::South:
      return {
        .x = point.y,
        .y = -point.x,
      };
  }

  throw std::invalid_argument(std::string{"value "} + std::to_string(static_cast<int>(direction)) + " not handled");
}

Vector rotateTo(double length, Direction direction)
{
  return rotate({length, 0}, direction);
}

std::size_t rotateCount(Direction direction)
{
  switch (direction) {
    case Direction::East:
      return 0;
    case Direction::North:
      return 1;
    case Direction::West:
      return 2;
    case Direction::South:
      return 3;
  }
  throw std::invalid_argument(std::string{"value "} + std::to_string(static_cast<int>(direction)) + " not handled");
}

Direction getDirection(bool horizontal, bool positive)
{
  if (horizontal) {
    return positive ? Direction::East : Direction::West;
  } else {
    return positive ? Direction::North : Direction::South;
  }
}

Direction getDirection(const Point& a, const Point& b)
{
  const auto dirVec = b - a;
  const bool horizontal = std::abs(dirVec.x) >= std::abs(dirVec.y);
  const auto positive = horizontal ? (dirVec.x >= 0) : (dirVec.y >= 0);
  return getDirection(horizontal, positive);
}

Direction rotateQuarter(Direction value)
{
  switch (value) {
    case Direction::East:
      return Direction::North;
    case Direction::North:
      return Direction::West;
    case Direction::West:
      return Direction::South;
    case Direction::South:
      return Direction::East;
  }
  throw std::invalid_argument(std::string{"value "} + std::to_string(static_cast<int>(value)) + " not handled");
}


}
