#pragma once

namespace geometry::_2d
{


struct Point {
  double x{};
  double y{};
};

struct Vector {
  double x{};
  double y{};
};

Vector operator-(const Vector&);
Vector operator/(const Vector&, double);

Point operator+(const Point&, const Vector&);
Vector operator-(const Point&, const Point&);

Point min(const Point&, const Point&);
Point max(const Point&, const Point&);

double length(const Vector&);


}
