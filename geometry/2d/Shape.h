#pragma once

#include "Point.h"
#include <vector>

namespace geometry::_2d
{


using Shape = std::vector<Point>;

Shape moveBy(const Shape&, const Vector&);
Point min(const Shape&);
Point max(const Shape&);


}
