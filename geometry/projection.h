#pragma once

#include "geometry/2d/Point.h"
#include "geometry/3d/Point.h"

namespace geometry
{


_3d::Point as3d(const _2d::Point& value, double z);
_3d::Vector as3d(const _2d::Vector& value, double z);
_2d::Point as2d(const _3d::Point& value);
_2d::Vector as2d(const _3d::Vector& value);


}
