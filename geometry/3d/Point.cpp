#include "Point.h"
#include <cmath>

namespace geometry::_3d
{


Vector operator-(const Point& left, const Point& right)
{
  return {
    .x = left.x - right.x,
    .y = left.y - right.y,
    .z = left.z - right.z,
  };
}

Point operator+(const Point& left, const Vector& right)
{
  return {
    .x = left.x + right.x,
    .y = left.y + right.y,
    .z = left.z + right.z,
  };
}

Vector operator/(const Vector& left, double right)
{
  return {
    .x = left.x / right,
    .y = left.y / right,
    .z = left.z / right,
  };
}

double length(const Vector& v)
{
  return std::sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}


}
