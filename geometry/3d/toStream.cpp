#include "toStream.h"

namespace geometry::_3d
{


std::ostream& operator<<(std::ostream& os, const Point& value)
{
  return os << "(" << value.x << ", " << value.y << ", " << value.z << ")";
}

std::ostream& operator<<(std::ostream& os, const Vector& value)
{
  return os << "(" << value.x << ", " << value.y << ", " << value.z << ")";
}


}
