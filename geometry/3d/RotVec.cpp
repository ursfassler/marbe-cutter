#include "RotVec.h"
#include "geometry/2d/rotate.h"

namespace geometry::_3d
{


Point operator+(const Point& base, const RotVec& delta)
{
  const auto delta2 = geometry::_2d::rotateTo(delta.xyLength, delta.direction);
  return {
    .x = base.x + delta2.x,
    .y = base.y + delta2.y,
    .z = base.z + delta.z,
  };
}


}
