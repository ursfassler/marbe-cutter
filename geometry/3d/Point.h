#pragma once

namespace geometry::_3d
{


struct Point {
  double x{};
  double y{};
  double z{};
};

struct Vector {
  double x{};
  double y{};
  double z{};
};

Vector operator-(const Point&, const Point&);
Point operator+(const Point&, const Vector&);
Vector operator/(const Vector&, double);
double length(const Vector&);


}
