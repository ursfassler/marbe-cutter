#pragma once

#include "geometry/2d/Direction.h"
#include "geometry/3d/Point.h"

namespace geometry::_3d
{


struct RotVec {
  geometry::_2d::Direction direction{};
  double xyLength{};
  double z{};
};


Point operator+(const Point&, const RotVec&);


}
