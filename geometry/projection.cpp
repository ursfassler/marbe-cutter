#include "projection.h"

namespace geometry
{


_3d::Point as3d(const _2d::Point& value, double z)
{
  return {
    .x = value.x,
    .y = value.y,
    .z = z,
  };
}

_3d::Vector as3d(const _2d::Vector& value, double z)
{
  return {
    .x = value.x,
    .y = value.y,
    .z = z,
  };
}

_2d::Point as2d(const _3d::Point& value)
{
  return {
    .x = value.x,
    .y = value.y,
  };
}

_2d::Vector as2d(const _3d::Vector& value)
{
  return {
    .x = value.x,
    .y = value.y,
  };
}


}
