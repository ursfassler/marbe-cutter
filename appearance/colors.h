#pragma once

#include "model/Color.h"

namespace appearance
{

using model::Color;


const Color Black = {0x00, 0x00, 0x00};
const Color Gray = {0x7f, 0x7f, 0x7f};
const Color Silver = {0xc0, 0xc0, 0xc0};
const Color Red = {0xff, 0x00, 0x00};
const Color Forestgreen = {0x22, 0x8b, 0x22};
const Color Royalblue = {0x41, 0x69, 0xe1};


}
