#include "Segments.h"
#include "geometry/projection.h"
#include <cmath>
#include <stdexcept>

namespace track
{


Segments createSegments(const Pillars& pillars)
{
  std::vector<Segment> result{};

  if (pillars.edges.empty()) {
    result.emplace_back(pillars.end, pillars.start);
  } else {
    result.emplace_back(pillars.end, pillars.edges.at(0));
    for(std::size_t i = 0; i < pillars.edges.size()-1; i++) {
      result.emplace_back(pillars.edges.at(i), pillars.edges.at(i+1));
    }
    result.emplace_back(pillars.edges.at(pillars.edges.size()-1), pillars.start);
  }

  return result;
}


Segment::Segment(
    const std::shared_ptr<PillarEntry>& exit,
    const std::shared_ptr<PillarExit>& entry
    ) :
  exitPillar_{exit},
  entryPillar_{entry}
{
}

const std::shared_ptr<PillarEntry>& Segment::exitPillar() const
{
  return exitPillar_;
}

const std::shared_ptr<PillarExit>& Segment::entryPillar() const
{
  return entryPillar_;
}

geometry::_2d::Direction Segment::direction() const
{
  return exitPillar_->entryOffset().direction;
}

double Segment::length() const
{
  const auto delta = entryPillar_->exitPosition() - exitPillar_->entryPosition();
  const auto xyLength = geometry::_2d::length(geometry::as2d(delta));
  return xyLength;
}

double Segment::slope() const
{
  const auto delta = entryPillar_->exitPosition() - exitPillar_->entryPosition();
  const auto xyLength = geometry::_2d::length(geometry::as2d(delta));
  const auto zLength = delta.z;
  return zLength / xyLength;
}

std::vector<geometry::_3d::Point> Segment::path() const
{
  return {
    entryPillar()->exitPosition(),
    exitPillar()->entryPosition(),
  };
}


}
