#include "Segments.h"
#include "Pillars.h"
#include "SimplisticPillar.h"
#include <gtest/gtest.h>

namespace track::test
{


class SegmentsTest :
    public ::testing::Test
{
protected:
  const Pillars pillars{
    .end = std::make_unique<SimplisticEnd>(geometry::_3d::Point{0, 0, 0}, geometry::_2d::Direction::South),
    .edges = { std::make_unique<SimplisticCorner>(geometry::_3d::Point{ 0, -20, 2 }, geometry::_2d::Direction::East, geometry::_2d::Direction::North) },
    .start = std::make_unique<SimplisticStart>(geometry::_3d::Point{ 50, -20, 12 }, geometry::_2d::Direction::West),
  };
  const Segments testee{createSegments(pillars)};
  const double eps{0.0001};
};


TEST_F(SegmentsTest, all_properties)
{
  ASSERT_EQ(testee.size(), 2);

  const auto s0 = testee.at(0);
  EXPECT_NEAR(s0.length(), 20, eps);
  EXPECT_NEAR(s0.slope(), 0.1, eps);
  EXPECT_EQ(s0.direction(),  geometry::_2d::Direction::South);

  const auto s1 = testee.at(1);
  EXPECT_NEAR(s1.length(), 50, eps);
  EXPECT_NEAR(s1.slope(), 0.2, eps);
  EXPECT_EQ(s1.direction(),  geometry::_2d::Direction::East);
}


}
