#pragma once

#include "geometry/2d/Direction.h"
#include "Pillars.h"
#include <vector>
#include <memory>

namespace track
{


class Segment
{
public:
  Segment(
      const std::shared_ptr<PillarEntry>& exit,
      const std::shared_ptr<PillarExit>& entry
      );

  const std::shared_ptr<PillarEntry>& exitPillar() const;
  const std::shared_ptr<PillarExit>& entryPillar() const;

  geometry::_2d::Direction direction() const;
  double length() const;
  double slope() const;

  std::vector<geometry::_3d::Point> path() const;

private:
  const std::shared_ptr<PillarEntry> exitPillar_;
  const std::shared_ptr<PillarExit> entryPillar_;
};


using Segments = std::vector<Segment>;

Segments createSegments(const Pillars&);


}
