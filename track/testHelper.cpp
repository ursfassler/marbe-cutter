#include "testHelper.h"

namespace track::test
{
namespace
{


constexpr double eps{0.0001};


}

testing::internal::AllOfMatcher<testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_2d::Point, double>>, testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_2d::Point, double>>>
EqP2(const geometry::_2d::Point& value)
{
  return testing::AllOf(
        testing::Field(&geometry::_2d::Point::x, testing::DoubleNear(value.x, eps)),
        testing::Field(&geometry::_2d::Point::y, testing::DoubleNear(value.y, eps))
        );
}

testing::internal::AllOfMatcher<
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_3d::Point, double>>,
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_3d::Point, double>>,
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_3d::Point, double>>
>
EqP3(const geometry::_3d::Point& value)
{
  return testing::AllOf(
        testing::Field(&geometry::_3d::Point::x, testing::DoubleNear(value.x, eps)),
        testing::Field(&geometry::_3d::Point::y, testing::DoubleNear(value.y, eps)),
        testing::Field(&geometry::_3d::Point::z, testing::DoubleNear(value.z, eps))
        );
}


}
