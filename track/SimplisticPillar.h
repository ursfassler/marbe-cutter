#pragma once

#include "Pillars.h"

namespace track
{


struct HeightParameter;


class SimplisticEnd :
    public PillarEntry
{
public:
  SimplisticEnd(
      const geometry::_3d::Point& position,
      geometry::_2d::Direction entryDirection
      );

  geometry::_2d::Point position() const override;
  double interlockHeight(double) const override;
  std::vector<geometry::_3d::Point> path() const override;
  geometry::_3d::Point entryPosition() const override;
  geometry::_3d::RotVec entryOffset() const override;
  std::optional<Tab> tab() const override;

private:
  const geometry::_3d::Point position_;
  const geometry::_2d::Direction entryDirection_;
};

class SimplisticStart :
    public PillarExit
{
public:
  SimplisticStart(
      const geometry::_3d::Point& position,
      geometry::_2d::Direction exitDirection
      );

  geometry::_2d::Point position() const override;
  double interlockHeight(double) const override;
  std::vector<geometry::_3d::Point> path() const override;
  geometry::_3d::Point exitPosition() const override;
  geometry::_3d::RotVec exitOffset() const override;
  std::optional<Tab> tab() const override;

private:
  const geometry::_3d::Point position_;
  const geometry::_2d::Direction exitDirection_;
};

class SimplisticCorner :
    public PillarCorner
{
public:
  SimplisticCorner(
      const geometry::_3d::Point& position,
      geometry::_2d::Direction entryDirection,
      geometry::_2d::Direction exitDirection
      );

  geometry::_2d::Point position() const override;
  double interlockHeight(double) const override;
  std::vector<geometry::_3d::Point> path() const override;
  geometry::_3d::Point exitPosition() const override;
  geometry::_3d::RotVec exitOffset() const override;
  geometry::_3d::Point entryPosition() const override;
  geometry::_3d::RotVec entryOffset() const override;
  std::optional<Tab> tab() const override;

private:
  const geometry::_3d::Point position_;
  const geometry::_2d::Direction entryDirection_;
  const geometry::_2d::Direction exitDirection_;
};


track::Pillars createPillars(const std::vector<double>&, const track::HeightParameter&);


}
