#pragma once

namespace track
{


struct HeightParameter
{
  double slope{};
  double endHeight{};
};


}
