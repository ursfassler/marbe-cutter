#pragma once

#include "geometry/2d/Point.h"
#include "geometry/3d/Point.h"
#include <gmock/gmock-matchers.h>

namespace track::test
{


testing::internal::AllOfMatcher<
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_2d::Point, double>>,
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_2d::Point, double>>
>
EqP2(const geometry::_2d::Point&);

testing::internal::AllOfMatcher<
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_3d::Point, double>>,
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_3d::Point, double>>,
  testing::PolymorphicMatcher<testing::internal::FieldMatcher<geometry::_3d::Point, double>>
>
EqP3(const geometry::_3d::Point&);


}
