#include "SimplisticPillar.h"
#include "HeightParameter.h"
#include "geometry/2d/toStream.h"
#include "geometry/3d/toStream.h"
#include "track/testHelper.h"
#include <gtest/gtest.h>

namespace track::test
{


class SimplisticPillarTest :
    public ::testing::Test
{
protected:
  const std::vector<double> segments{
    60,
    40,
    -90,
  };
  const HeightParameter heightParameter {
    .slope = 0.1,
    .endHeight = 30,
  };

  const Pillars testee{createPillars(segments, heightParameter)};

  const double eps{0.0001};
};


TEST_F(SimplisticPillarTest, all_properties)
{
  ASSERT_EQ(testee.edges.size(), 2);

  const auto p0 = testee.end;
  EXPECT_THAT(p0->position(), EqP2({0, 0}));
  EXPECT_EQ(p0->entryOffset().direction,  geometry::_2d::Direction::East);
  EXPECT_NEAR(p0->entryOffset().xyLength, 0, eps);
  EXPECT_THAT(p0->entryPosition(), EqP3({0, 0, 30}));

  const auto p1 = testee.edges.at(0);
  EXPECT_THAT(p1->position(), EqP2({60, 0}));
  EXPECT_EQ(p1->exitOffset().direction,  geometry::_2d::Direction::West);
  EXPECT_NEAR(p1->exitOffset().xyLength, 0, eps);
  EXPECT_THAT(p1->exitPosition(), EqP3({60, 0, 36}));
  EXPECT_EQ(p1->entryOffset().direction,  geometry::_2d::Direction::North);
  EXPECT_NEAR(p1->entryOffset().xyLength, 0, eps);
  EXPECT_THAT(p1->entryPosition(), EqP3({60, 0, 36}));

  const auto p2 = testee.edges.at(1);
  EXPECT_THAT(p2->position(), EqP2({60, 40}));
  EXPECT_EQ(p2->exitOffset().direction,  geometry::_2d::Direction::South);
  EXPECT_NEAR(p2->exitOffset().xyLength, 0, eps);
  EXPECT_THAT(p2->exitPosition(), EqP3({60, 40, 40}));
  EXPECT_EQ(p2->entryOffset().direction,  geometry::_2d::Direction::West);
  EXPECT_NEAR(p2->entryOffset().xyLength, 0, eps);
  EXPECT_THAT(p2->entryPosition(), EqP3({60, 40, 40}));

  const auto p3 = testee.start;
  EXPECT_THAT(p3->position(), EqP2({-30, 40}));
  EXPECT_EQ(p3->exitOffset().direction,  geometry::_2d::Direction::East);
  EXPECT_NEAR(p3->exitOffset().xyLength, 0, eps);
  EXPECT_THAT(p3->exitPosition(), EqP3({-30, 40, 49}));
}


}
