#pragma once

#include "geometry/2d/Point.h"
#include "geometry/3d/Point.h"
#include "geometry/3d/RotVec.h"
#include <optional>
#include <vector>
#include <memory>

namespace track
{


struct Tab
{
  geometry::_2d::Point position{};
  geometry::_2d::Direction direction{};
};


class Pillar
{
public:
  virtual ~Pillar() = default;

  virtual geometry::_2d::Point position() const = 0;
  virtual double interlockHeight(double axleHeight) const = 0; //TODO remove
  virtual std::vector<geometry::_3d::Point> path() const = 0;
  virtual std::optional<Tab> tab() const = 0;
};

class PillarEntry :
    public Pillar //TODO remove parent
{
public:
  virtual ~PillarEntry() = default;

  virtual geometry::_3d::Point entryPosition() const = 0;
  virtual geometry::_3d::RotVec entryOffset() const = 0;
};

class PillarExit :
    public Pillar //TODO remove parent
{
public:
  virtual ~PillarExit() = default;

  virtual geometry::_3d::Point exitPosition() const = 0;
  virtual geometry::_3d::RotVec exitOffset() const = 0;
};

class PillarCorner :
    public virtual PillarEntry,
    public virtual PillarExit
{
public:
  virtual geometry::_2d::Point position() const = 0;
  virtual double interlockHeight(double axleHeight) const = 0;
  virtual std::vector<geometry::_3d::Point> path() const = 0;
  virtual std::optional<Tab> tab() const = 0;
};

struct Pillars
{
  std::shared_ptr<PillarEntry> end{};
  std::vector<std::shared_ptr<PillarCorner>> edges{};
  std::shared_ptr<PillarExit> start{};
};


}
