#include "SimplisticPillar.h"
#include "geometry/2d/rotate.h"
#include "geometry/projection.h"
#include "track/HeightParameter.h"

namespace track
{
namespace
{


geometry::_2d::Direction entry(std::size_t i, const std::vector<geometry::_2d::Direction>& entryDirections)
{
  return entryDirections.at(i);
}

geometry::_2d::Direction exit(std::size_t i, const std::vector<geometry::_2d::Direction>& entryDirections)
{
  return rotateQuarter(rotateQuarter(entryDirections.at(i-1)));
}


}

SimplisticEnd::SimplisticEnd(const geometry::_3d::Point& position, geometry::_2d::Direction entryDirection) :
  position_{position},
  entryDirection_{entryDirection}
{
}

geometry::_2d::Point SimplisticEnd::position() const
{
  return geometry::as2d(position_);
}

double SimplisticEnd::interlockHeight(double) const
{
  return {};
}

std::vector<geometry::_3d::Point> SimplisticEnd::path() const
{
  return { position_ };
}

geometry::_3d::Point SimplisticEnd::entryPosition() const
{
  return position_;
}

geometry::_3d::RotVec SimplisticEnd::entryOffset() const
{
  return { entryDirection_, 0, 0 };
}

std::optional<Tab> SimplisticEnd::tab() const
{
  return {};
}



SimplisticStart::SimplisticStart(
    const geometry::_3d::Point& position,
    geometry::_2d::Direction exitDirection
    ) :
  position_{position},
  exitDirection_{exitDirection}
{
}

geometry::_2d::Point SimplisticStart::position() const
{
  return geometry::as2d(position_);
}

double SimplisticStart::interlockHeight(double) const
{
  return {};
}

std::vector<geometry::_3d::Point> SimplisticStart::path() const
{
  return { position_ };
}

geometry::_3d::Point SimplisticStart::exitPosition() const
{
  return position_;
}

geometry::_3d::RotVec SimplisticStart::exitOffset() const
{
  return { exitDirection_, 0, 0 };
}

std::optional<Tab> SimplisticStart::tab() const
{
  return {};
}



SimplisticCorner::SimplisticCorner(
    const geometry::_3d::Point& position,
    geometry::_2d::Direction entryDirection,
    geometry::_2d::Direction exitDirection
    ) :
  position_{position},
  entryDirection_{entryDirection},
  exitDirection_{exitDirection}
{
}

geometry::_2d::Point SimplisticCorner::position() const
{
  return geometry::as2d(position_);
}

double SimplisticCorner::interlockHeight(double) const
{
  return {};
}

std::vector<geometry::_3d::Point> SimplisticCorner::path() const
{
  return { position_ };
}

geometry::_3d::Point SimplisticCorner::exitPosition() const
{
  return position_;
}

geometry::_3d::RotVec SimplisticCorner::exitOffset() const
{
  return { exitDirection_, 0, 0 };
}

geometry::_3d::Point SimplisticCorner::entryPosition() const
{
  return position_;
}

geometry::_3d::RotVec SimplisticCorner::entryOffset() const
{
  return { entryDirection_, 0, 0 };
}

std::optional<Tab> SimplisticCorner::tab() const
{
  return {};
}


track::Pillars createPillars(const std::vector<double>& segments, const track::HeightParameter& heightParameter)
{
  std::vector<geometry::_3d::Point> positions{};
  std::vector<geometry::_2d::Direction> entryDirections{};

  geometry::_3d::Point pos{0, 0, heightParameter.endHeight};

  positions.push_back(pos);
  for(std::size_t i = 0; i < segments.size(); i++) {
    const auto length = segments[i];
    const auto horizontal = (i % 2) == 0;
    const auto positive = length > 0;
    const auto direction = geometry::_2d::getDirection(horizontal, positive);

    const auto deltaHeight = std::abs(length) * heightParameter.slope;
    const auto deltaPos2d = geometry::_2d::rotateTo(std::abs(length), direction);

    pos = pos + geometry::as3d(deltaPos2d, deltaHeight);

    entryDirections.push_back(direction);
    positions.push_back(pos);
  }

  track::Pillars result{};

  {
    const auto i = 0;
    result.end = std::make_shared<SimplisticEnd>(positions.at(i), entry(i, entryDirections));
  }

  for (std::size_t i = 1; i < positions.size()-1; i++) {
    result.edges.push_back(std::make_shared<SimplisticCorner>(positions.at(i), entry(i, entryDirections), exit(i, entryDirections)));
  }

  {
    const auto i = positions.size()-1;
    result.start = std::make_shared<SimplisticStart>(positions.at(i), exit(i, entryDirections));
  }

  return result;
}


}
