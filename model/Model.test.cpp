#include "Model.h"
#include "geometry/3d/toStream.h"
#include "track/testHelper.h"
#include "track/SimplisticPillar.h"
#include <gtest/gtest.h>

namespace model::test
{

using track::test::EqP3;


class ModelTest :
    public ::testing::Test
{
protected:
  const shape::MarbleDiameter diameter
  {
    .normal = 16,
    .minimum = 15,
    .maximum = 17,
  };
  const shape::TrackData trackData
  {
    .gauge = 10,
    .width = 0.5,
  };
  const shape::TabData tabData
  {
    .length = 10,
    .deepthReduction = 0.2,
  };
  const track::Pillars pillars{
    .end = std::make_unique<track::SimplisticEnd>(geometry::_3d::Point{0, 0, 0}, geometry::_2d::Direction{}),
    .edges = { std::make_unique<track::SimplisticCorner>(geometry::_3d::Point{ 0, -20, 2 }, geometry::_2d::Direction{}, geometry::_2d::Direction{}) },
    .start = std::make_unique<track::SimplisticStart>(geometry::_3d::Point{ 50, -20, 12 }, geometry::_2d::Direction{}),
  };
  track::Segments segments{createSegments(pillars)};
  shape::Shapes shapes{};

  Model testee{trackData, tabData, diameter, pillars, segments, shapes};

  const double eps{0.0001};
};


TEST_F(ModelTest, create_base)
{
  const auto size = testee.getBaseSize();

  EXPECT_THAT(size, EqP3({50, 0.5, 20}));
}

TEST_F(ModelTest, create_marblePath)
{
  const auto path = testee.marblePath();

  ASSERT_EQ(path.size(), 3);
  EXPECT_THAT(path[0], EqP3({50, -20, 12 }));
  EXPECT_THAT(path[1], EqP3({ 0, -20,  2 }));
  EXPECT_THAT(path[2], EqP3({ 0,   0,  0 }));
}


}
