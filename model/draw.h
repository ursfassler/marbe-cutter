#pragma once

#include "Color.h"
#include <functional>

namespace geometry::_2d
{

struct IndexedShapeWithHoles;
struct ShapeWithHoles;

}

namespace model
{

class Model;
class ModelBuilder;

using Tessellate = std::function<geometry::_2d::IndexedShapeWithHoles(const geometry::_2d::ShapeWithHoles&)>;

struct Colors
{
  Color background;
  Color marker;
  Color marble;
  Color shape;
};

void drawModel(const Model&, const Colors&, const Tessellate&, ModelBuilder&);


}
