#include "createSimplisticModel.h"
#include "shape/createShapes.h"
#include "track/SimplisticPillar.h"

namespace model
{


Model createSimplisticModel(
    const model::SegmentLengths& segmentLengths,
    const track::HeightParameter& heightParameter,
    const shape::TrackData& trackData,
    const shape::TabData& tabData,
    const shape::MarbleDiameter& marbleDiameter
    )
{
  track::Pillars pillars{track::createPillars(segmentLengths, heightParameter)};
  track::Segments segments{createSegments(pillars)};
  shape::Shapes shapes{shape::createShapes(segments, trackData.gauge, marbleDiameter.normal / 2)};

  return {trackData, tabData, marbleDiameter, pillars, segments, shapes};
}


}
