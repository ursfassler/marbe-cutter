#include "Model.h"
#include "geometry/2d/rotate.h"
#include "geometry/projection.h"
#include <cmath>

using geometry::_2d::operator+;
using geometry::_2d::operator-;

namespace model
{
namespace
{


struct BaseTab
{
  geometry::_2d::Point p1{};
  geometry::_2d::Point p2{};
};

BaseTab placeTab(const BaseTab& value, const track::Tab& trackTab)
{
  const geometry::_2d::Point zero{0, 0};

  const auto orgP1 = value.p1 - zero;
  const auto rotP1 = rotate(orgP1, trackTab.direction);

  const auto orgP2 = value.p2 - zero;
  const auto rotP2 = rotate(orgP2, trackTab.direction);

  return {
    .p1 = trackTab.position + rotP1,
    .p2 = trackTab.position + rotP2,
  };
}

geometry::_2d::Shape createHole(const BaseTab& tab)
{
  geometry::_2d::Point min = geometry::_2d::min(tab.p1, tab.p2);
  geometry::_2d::Point max = geometry::_2d::max(tab.p1, tab.p2);

  geometry::_2d::Shape hole{};
  hole.push_back({max.x, min.y});
  hole.push_back({max.x, max.y});
  hole.push_back({min.x, max.y});
  hole.push_back({min.x, min.y});

  return hole;
}

std::pair<BaseTab, BaseTab> createTabs(const DoubleTab& tab, const track::Tab& trackTab)
{
  const BaseTab tab1{
    .p1={.x=-tab.length/2,.y=tab.distance/2},
    .p2={.x=tab.length/2,.y=tab.distance/2+tab.width},
  };

  const BaseTab tab2{
    .p1={.x=-tab.length/2,.y=-tab.distance/2-tab.width},
    .p2={.x=tab.length/2,.y=-tab.distance/2},
  };

  return {
    placeTab(tab1, trackTab),
    placeTab(tab2, trackTab),
  };
}

std::pair<geometry::_2d::Shape, geometry::_2d::Shape> createTabHoles(const shape::TrackData& trackData, const shape::TabData& tabData, const track::Tab& trackTab)
{
  const DoubleTab tab{
    .distance=trackData.gauge,
    .width=trackData.width,
    .length=tabData.length,
  };
  const auto tabs = createTabs(tab, trackTab);
  return {
    createHole(tabs.first),
    createHole(tabs.second),
  };
}


}


Model::Model(
    const shape::TrackData& trackData_,
    const shape::TabData& tabData_,
    const shape::MarbleDiameter& marbleDiameter_,
    const track::Pillars& pillars_,
    const track::Segments& segments_,
    const shape::Shapes& shapes_
    ) :
  trackData{trackData_},
  tabData{tabData_},
  diameter{marbleDiameter_},
  pillars{pillars_},
  segments{segments_},
  shapes{shapes_}
{
  endPosition = geometry::as3d(pillars.end->position(), 0);
  for (std::size_t index = 0; index < segments.size(); index++) {
    segmentPosition.push_back(geometry::as3d(segments.at(index).exitPillar()->position(), 0));
  }
  startPosition = geometry::as3d(pillars.start->position(), 0);
}

void Model::forEachPillar(const std::function<void (const geometry::_2d::Point&)>& func) const
{
  func(pillars.end->position());
  for (std::size_t i = 0; i < pillars.edges.size(); i++) {
    func(pillars.edges.at(i)->position());
  }
  func(pillars.start->position());
}

std::size_t Model::getSegmentCount() const
{
  return shapes.segments.size();
}

geometry::_3d::Point Model::getBaseSize() const
{
  const auto shape = getBaseShape();
  const auto min = geometry::_2d::min(shape.outline);
  const auto max = geometry::_2d::max(shape.outline);

  const auto size = max - min;
  return {
    .x = size.x,
    .y = trackData.width,
    .z = size.y,
  };
}

geometry::_2d::ShapeWithHoles Model::getBaseShape() const
{
  geometry::_2d::Point min{0, 0};
  geometry::_2d::Point max{0, 0};

  forEachTrack([&](const Track& track){
    const auto pos = geometry::as2d(track.position);
    const auto minX = geometry::_2d::min(track.shape).x;
    const auto maxX = geometry::_2d::max(track.shape).x;
    const auto p1 = pos + rotateTo(minX, track.direction);
    const auto p2 = pos + rotateTo(maxX, track.direction);
    min = geometry::_2d::min(p1, min);
    max = geometry::_2d::max(p1, max);
    min = geometry::_2d::min(p2, min);
    max = geometry::_2d::max(p2, max);
  });

  geometry::_2d::ShapeWithHoles shape{};

  shape.outline.push_back({min.x, min.y});
  shape.outline.push_back({min.x, max.y});
  shape.outline.push_back({max.x, max.y});
  shape.outline.push_back({max.x, min.y});

  std::vector<track::Tab> tabs{};

  if (const auto tab = pillars.end->tab(); tab) {
    tabs.push_back(*tab);
  }

  for (const auto& pillar : pillars.edges) {
    if (const auto tab = pillar->tab(); tab) {
      tabs.push_back(*tab);
    }
  }

  if (const auto tab = pillars.start->tab(); tab) {
    tabs.push_back(*tab);
  }

  for (const auto &tab : tabs) {
    const auto tabHoles = createTabHoles(trackData, tabData, tab);
    shape.holes.push_back(tabHoles.first);
    shape.holes.push_back(tabHoles.second);
  }

  return shape;
}

void Model::forEachShape(const std::function<void (const geometry::_2d::ShapeWithHoles&, std::size_t times)>& func) const
{
  func(getBaseShape(), 1);
  func({.outline=shapes.end}, 2);
  for (const auto& shape: shapes.segments) {
    func({.outline=shape}, 2);
  }
  func({.outline=shapes.start}, 2);
}

Track Model::getEndTrack() const
{
  return {
    .shape = shapes.end,
        .position = endPosition,
        .direction = geometry::_2d::Direction::North,
        .data = trackData,
  };
}

Track Model::getSegmentTrack(std::size_t index) const
{
  return {
    .shape = shapes.segments.at(index),
        .position = segmentPosition.at(index),
        .direction = segments.at(index).direction(),
        .data = trackData,
  };
}

Track Model::getStartTrack() const
{
  return {
    .shape = shapes.start,
        .position = startPosition,
        .direction = geometry::_2d::rotateQuarter(segments.back().direction()),
        .data = trackData,
  };
}

void Model::forEachTrack(const std::function<void (const Track&)>& func) const
{
  func(getEndTrack());
  for (std::size_t i = 0; i < getSegmentCount(); i++) {
    func(getSegmentTrack(i));
  }
  func(getStartTrack());
}

Path Model::marblePath() const
{
  Path result{};

  for (auto segment = segments.crbegin(); segment != segments.crend(); segment++) {
    const auto sp = segment->entryPillar()->path();
    result.insert(result.end(), sp.begin(), sp.end());

    const auto pp = segment->path();
    result.insert(result.end(), pp.begin()+1, pp.end()-1);
  }

  const auto pp = pillars.end->path();
  result.insert(result.end(), pp.begin(), pp.end());

  return result;
}

double Model::trackGauge() const
{
  return trackData.gauge;
}

double Model::marbleDiameter() const
{
  return diameter.normal;
}


}
