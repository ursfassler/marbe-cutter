#pragma once

#include "geometry/2d/Shape.h"
#include "geometry/2d/ShapeWithHoles.h"
#include "geometry/3d/Point.h"
#include "shape/MarbleDiameter.h"
#include "shape/Shapes.h"
#include "shape/TabData.h"
#include "shape/TrackData.h"
#include "track/Segments.h"
#include <functional>
#include <vector>

namespace model
{


using SegmentLengths = std::vector<double>;
using Path = std::vector<geometry::_3d::Point>;


struct Track
{
  geometry::_2d::Shape shape{};
  geometry::_3d::Point position{};
  geometry::_2d::Direction direction{};
  shape::TrackData data{};
};

struct DoubleTab
{
  double distance{};
  double width{};
  double length{};
};

class Model
{
public:
  Model(
      const shape::TrackData&,
      const shape::TabData&,
      const shape::MarbleDiameter&,
      const track::Pillars&,
      const track::Segments&,
      const shape::Shapes&
      );

  geometry::_3d::Point getBaseSize() const;
  geometry::_2d::ShapeWithHoles getBaseShape() const;

  void forEachPillar(const std::function<void(const geometry::_2d::Point&)>&) const;
  void forEachShape(const std::function<void(const geometry::_2d::ShapeWithHoles&, std::size_t times)>&) const;
  void forEachTrack(const std::function<void(const Track&)>&) const;
  Path marblePath() const;

  double trackGauge() const;
  double marbleDiameter() const;

private:
  const shape::TrackData trackData;
  const shape::TabData tabData;
  const shape::MarbleDiameter diameter;
  const track::Pillars pillars;
  const track::Segments segments;
  const shape::Shapes shapes;
  geometry::_3d::Point endPosition{};
  std::vector<geometry::_3d::Point> segmentPosition{};
  geometry::_3d::Point startPosition{};

  std::size_t getSegmentCount() const;
  Track getEndTrack() const;
  Track getSegmentTrack(std::size_t) const;
  Track getStartTrack() const;
};


}
