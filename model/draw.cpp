#include "draw.h"
#include "Model.h"
#include "ModelBuilder.h"
#include "geometry/2d/Point.h"
#include "geometry/2d/rotate.h"
#include "geometry/3d/Point.h"
#include "model/Color.h"
#include <array>
#include <cmath>
#include <fstream>
#include <functional>
#include <map>
#include <vector>

namespace model
{
namespace
{


void beginQuarterRotation(std::size_t value, Dimension dimension, ModelBuilder& b)
{
  b.beginRotation(value * std::numbers::pi / 2, dimension);
}

void endQuarterRotation(ModelBuilder& b)
{
  b.endRotation();
}

void drawSide(const geometry::_2d::IndexedShapeWithHoles& shape, double from, double deepth, const Color& color, ModelBuilder& b)
{
  b.beginTranslate({0, 0, from});
  b.extrude(shape, deepth, color);
  b.endTranslate();
}

void drawTrack(const Track& track, const Tessellate& tessellate, const Color& color, ModelBuilder& b)
{
  b.beginTranslate(track.position);
  beginQuarterRotation(geometry::_2d::rotateCount(track.direction), Dimension::Z, b);
  beginQuarterRotation(1, Dimension::X, b);

  const auto shape = tessellate({.outline=track.shape});
  drawSide(shape, -track.data.gauge/2, -track.data.width, color, b);
  drawSide(shape, +track.data.gauge/2, +track.data.width, color, b);

  endQuarterRotation(b);
  endQuarterRotation(b);
  b.endTranslate();
}

void drawPath(const geometry::_3d::Point& from, const geometry::_3d::Point& to, double marbeDiameter, const Color& color, ModelBuilder& b)
{
  const auto diff = to - from;
  const auto midPoint = from + (diff / 2);
  const auto pathLength = length(diff);
  const auto heightDiff = -diff.z;
  const auto slopeRotation = std::asin(heightDiff / pathLength);

  const geometry::_2d::Vector diff2d{.x=diff.x, .y=diff.y};
  const auto xyRotation = std::asin(diff2d.x / length(diff2d)) + ((diff2d.y > 0) ? std::numbers::pi : 0);

  b.beginTranslate(midPoint);
  b.beginRotation(xyRotation, Dimension::Z);
  b.beginRotation(slopeRotation + std::numbers::pi/2, Dimension::X);

  b.cylinder(marbeDiameter/2, pathLength, color);

  b.endRotation();
  b.endRotation();
  b.endTranslate();
}


}


void drawModel(const Model& model, const Colors& colors, const Tessellate& tessellate, ModelBuilder& b)
{
  b.beginDocument("track", colors.background);

  const auto sideSize = model.trackGauge();
  b.cube({sideSize, sideSize, sideSize}, colors.marker);

  const auto shape = tessellate(model.getBaseShape());
  b.extrude(shape, -model.getBaseSize().y, colors.shape);

  model.forEachTrack([&](const Track& track){
    drawTrack(track, tessellate, colors.shape, b);
  });

  const auto path = model.marblePath();
  b.beginTranslate(path.front());
  b.sphere(model.marbleDiameter()/2, colors.marble);
  b.endTranslate();

  for (std::size_t i = 0; i < path.size()-1; i++) {
    drawPath(path.at(i), path.at(i+1), model.marbleDiameter(), colors.marble, b);
  }

  b.beginTranslate(path.back());
  b.sphere(model.marbleDiameter()/2, colors.marble);
  b.endTranslate();

  b.endDocument();
}


}
