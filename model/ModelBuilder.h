#pragma once

#include "Color.h"
#include "geometry/3d/Point.h"
#include "geometry/2d/IndexedShapeWithHoles.h"
#include <string>

namespace model
{


enum class Dimension
{
  X,
  Y,
  Z,
};


class ModelBuilder
{
public:
  virtual ~ModelBuilder() = default;

  virtual void beginDocument(const std::string& name, const Color& background) = 0;
  virtual void endDocument() = 0;

  virtual void extrude(const geometry::_2d::IndexedShapeWithHoles&, double deepth, const Color&) = 0;
  virtual void cube(const geometry::_3d::Point&, const Color&) = 0;
  virtual void cylinder(double radius, double height, const Color&) = 0;
  virtual void sphere(double radius, const Color&) = 0;

  virtual void beginRotation(double, Dimension) = 0;
  virtual void endRotation() = 0;

  virtual void beginTranslate(const geometry::_3d::Point&) = 0;
  virtual void endTranslate() = 0;
};


}
