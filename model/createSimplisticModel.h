#pragma once

#include "Model.h"

namespace track
{

struct HeightParameter;

}

namespace model
{


Model createSimplisticModel(
    const model::SegmentLengths&,
    const track::HeightParameter&,
    const shape::TrackData&,
    const shape::TabData&,
    const shape::MarbleDiameter&
    );


}
