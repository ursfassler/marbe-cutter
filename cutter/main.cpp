#include "cutter.h"
#include "model/Model.h"
#include <fstream>
#include <sstream>
#include <tclap/CmdLine.h>

namespace cutter
{
namespace
{


model::SegmentLengths parseSegments(const std::string& value)
{
  model::SegmentLengths result{};

  std::stringstream ss{value};
  std::string str{};

  while (getline(ss, str, ',')) {
    const double length = std::stod(str);
    result.push_back(length);
  }

  return result;
}


using ModelCreator = std::function<model::Model(const model::SegmentLengths&)>;
using ModelCreatorItem = std::pair<std::string, ModelCreator>;

const std::vector<ModelCreatorItem> modelCreator {
  {"drop", createDropModel},
  {"simplistic", createSimplisticModel},
};

std::string defaultType()
{
  return modelCreator.front().first;
}

std::string allTypes()
{
  std::string result{};

  bool first = true;
  for (const auto& itr : modelCreator) {
    if (first) {
      first = false;
    } else {
      result += ", ";
    }
    result += itr.first;
  }

  return result;
}

ModelCreator parseType(const std::string& type)
{
  const auto itr = std::find_if(modelCreator.begin(), modelCreator.end(), [type](const ModelCreatorItem& item){
    return item.first == type;
  });

  if (itr == modelCreator.end()) {
    throw std::invalid_argument("unknown pillar type: " + type);
  }

  return itr->second;
}


}


int main(int argc, char *argv[])
{
  model::SegmentLengths segments{};
  ModelCreator modelCreator{};

  try {
    TCLAP::CmdLine cmd{"create marble-run shapes for cutters"};

    TCLAP::ValueArg<std::string> segmentsArg("s", "segments", "segments of the marble-run", true, {}, "comma separated list of numbers");
    cmd.add(segmentsArg);

    TCLAP::ValueArg<std::string> pillarArg("p", "pillar", "type of pillar", false, defaultType(), allTypes());
    cmd.add(pillarArg);

    cmd.parse(argc, argv);

    segments = parseSegments(segmentsArg.getValue());
    modelCreator = parseType(pillarArg.getValue());
  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return -1;
  }

  const model::Model model{modelCreator(segments)};

  std::ofstream path{"path.svg"};
  writePath(path, model);

  std::ofstream sides{"sideSegments.svg"};
  writeSideSegments(sides, model);

  std::ofstream x3d{"model.x3d"};
  writeX3dModel(x3d, model);

  return 0;
}


}
