#pragma once

#include "model/Model.h"

namespace cutter
{


model::Model createDropModel(const model::SegmentLengths&);
model::Model createSimplisticModel(const model::SegmentLengths&);

void writePath(std::ostream&, const model::Model&);
void writeSideSegments(std::ostream&, const model::Model&);
void writeX3dModel(std::ostream&, const model::Model&);


}
