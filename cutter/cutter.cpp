#include "cutter.h"
#include "appearance/colors.h"
#include "corner/drop/DropPillars.h"
#include "corner/drop/createModel.h"
#include "geometry/2d/Point.h"
#include "geometry/2d/Shape.h"
#include "geometry/projection.h"
#include "model/Model.h"
#include "model/createSimplisticModel.h"
#include "model/draw.h"
#include "output/Canvas.h"
#include "output/X3dBuilder.h"
#include "shape/MarbleDiameter.h"
#include "tessellation/tessellator.h"
#include "track/HeightParameter.h"
#include <array>
#include <cmath>
#include <functional>
#include <map>
#include <vector>

namespace cutter
{
namespace
{


const track::HeightParameter heightParameter
{
  .slope = 0.05,
      .endHeight = 15,
};

const corner::drop::CornerSpacing cornerSpacing
{

  .width = 5,
      .height = 5,
};

const shape::MarbleDiameter diameter
{
  .normal = 16,
      .minimum = 15,
      .maximum = 17,
};

const shape::TrackData trackData
{
  .gauge = 10,
      .width = 0.5,
};

const shape::TabData tabData
{
  .length = 10,
      .deepthReduction = 0.2,
};


constexpr double spacing = 5;

const output::Stroke pathStroke = {
  .width = spacing/2,
  .color = appearance::Red,
};

const output::Area areaStyle = {
  .fill = appearance::Silver,
  .stroke = {
    .width = 0.2,
    .color = appearance::Black,
  },
};

const model::Colors colors = {
  .background = appearance::Royalblue,
  .marker = appearance::Forestgreen,
  .marble = appearance::Royalblue,
  .shape = appearance::Silver,
};

void drawZ(const auto path, output::Canvas& canvas)
{
  double x = 0;
  geometry::_2d::Point last{};
  bool first = true;

  geometry::_2d::Shape pathShape{};
  for (const auto& point : path) {
    const auto current = geometry::as2d(point);
    if (first) {
      first  = false;
    } else {
      const auto delta = geometry::_2d::length(last - current);
      x = x + delta;
    }
    last = current;

    pathShape.push_back({x, point.z});
  }
  canvas.addPath(pathShape, pathStroke);
}

void drawXy(const auto path, output::Canvas& canvas)
{
  geometry::_2d::Shape pathShape{};
  for (const auto& point : path) {
    pathShape.push_back(geometry::as2d(point));
  }
  canvas.addPath(pathShape, pathStroke);
}

void drawTrack(const model::Model& model, output::Canvas& canvas)
{
  const auto path = model.marblePath();

  drawZ(path, canvas);
  drawXy(path, canvas);
}


}

model::Model createDropModel(const model::SegmentLengths& segments)
{
  return corner::drop::createModel(segments, heightParameter, trackData, tabData, diameter, cornerSpacing);
}

model::Model createSimplisticModel(const model::SegmentLengths& segments)
{
  return model::createSimplisticModel(segments, heightParameter, trackData, tabData, diameter);
}

void writePath(std::ostream& stream, const model::Model& model)
{
  output::Canvas canvas{spacing};
  drawTrack(model, canvas);
  output::XmlWriter xml{stream};
  output::SvgWriter svg{xml};
  canvas.printSvg(svg);
}

void writeSideSegments(std::ostream& stream, const model::Model& model)
{
  output::Canvas canvas{spacing};
  model.forEachShape([&canvas](const geometry::_2d::ShapeWithHoles& shape, std::size_t times){
    canvas.addArea(shape, areaStyle, times);
  });
  output::XmlWriter xml{stream};
  output::SvgWriter svg{xml};
  canvas.printSvg(svg);
}

void writeX3dModel(std::ostream& stream, const model::Model& model)
{
  output::XmlWriter xml{stream};
  output::X3dBuilder b{xml};
  drawModel(model, colors, tessellation::tessellate, b);
}


}
