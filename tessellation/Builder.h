#pragma once

#include "geometry/2d/IndexedShapeWithHoles.h"
#include <GL/glu.h>
#include <cstddef>

namespace tessellation
{


class Builder
{
public:
  void begin(GLenum which);
  void end();
  void vertex(std::size_t index);
  void error(const char* error);
  const geometry::_2d::TrianglesIndices& getTriangles() const;

private:
  enum class Type {
    Strip,
    Plain,
    Fan,
  };
  Type type{};
  std::size_t vertexNumber{};
  std::size_t fanHubIndex{};
  geometry::_2d::TriangleIndices triangle{};
  geometry::_2d::TrianglesIndices triangles{};
  Type getType(GLenum which);

};


}
