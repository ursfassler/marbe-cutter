#pragma once

#include "geometry/2d/IndexedShapeWithHoles.h"
#include "geometry/2d/ShapeWithHoles.h"

namespace tessellation
{


geometry::_2d::IndexedShapeWithHoles tessellate(const geometry::_2d::ShapeWithHoles&);


}
