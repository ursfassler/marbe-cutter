#include "tessellation/Builder.h"
#include <array>
#include <stdexcept>
#include <vector>

namespace tessellation
{


void Builder::begin(GLenum which)
{
  type = getType(which);
  vertexNumber = 0;
}

void Builder::end()
{
}

void Builder::vertex(std::size_t index)
{
  triangle[0] = triangle[1];
  triangle[1] = triangle[2];
  triangle[2] = index;

  switch (type) {
    case Type::Plain:
      if ((vertexNumber % 3) == 2) {
        triangles.push_back(triangle);
      }
      break;

    case Type::Strip:
      if (vertexNumber == 2) {
        triangles.push_back(triangle);
      } else if (vertexNumber > 2) {
        bool reverse = (vertexNumber % 2) == 1;
        if (reverse) {
          triangles.push_back({triangle[1], triangle[0], index});
        } else {
          triangles.push_back({triangle[0], triangle[1], index});
        }
      }
      break;

    case Type::Fan:
      if (vertexNumber == 0) {
        fanHubIndex = index;
      } else if (vertexNumber == 2) {
        triangles.push_back(triangle);
      } else if (vertexNumber > 2) {
        triangles.push_back({fanHubIndex, triangle[1], index});
      }
      break;
  }

  vertexNumber++;
}

void Builder::error(const char* error)
{
  throw std::runtime_error(error);
}

const geometry::_2d::TrianglesIndices& Builder::getTriangles() const
{
  return triangles;
}

Builder::Type Builder::getType(GLenum which)
{
  switch (which) {
    case GL_TRIANGLES:
      return Type::Plain;
    case GL_TRIANGLE_STRIP:
      return Type::Strip;
    case GL_TRIANGLE_FAN:
      return Type::Fan;
  }

  throw std::runtime_error("unsupported type " + std::to_string(which));
}


}
