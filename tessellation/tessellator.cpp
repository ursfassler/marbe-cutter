#include "Builder.h"
#include "tessellator.h"
#include <GL/glu.h>
#include <array>
#include <stdexcept>
#include <vector>

namespace tessellation
{
namespace
{


struct VertexAndIndex{
  double v[3];
  std::size_t i;
};
using Shape = std::vector<VertexAndIndex>;
using Shapes = std::vector<Shape>;


void vertexCallback(std::size_t *i, Builder *builder)
{
  builder->vertex(*i);
}

void error(GLenum error, Builder *builder)
{
  const auto message = gluErrorString(error);
  builder->error(reinterpret_cast<const char*>(message));
}

void tess_begin(GLenum which, Builder *builder)
{
  builder->begin(which);
}

void tess_end(Builder *builder)
{
  builder->end();
}


}


geometry::_2d::IndexedShapeWithHoles tessellate(const geometry::_2d::ShapeWithHoles& shape)
{

  Shapes holes{};

  geometry::_2d::IndexedShapeWithHoles result{};

  holes.push_back({});
  for (auto& v : shape.outline) {
    const auto index = result.points.size();
    holes.back().push_back({.v={v.x, v.y, 0}, .i=index});
    result.outline.push_back(index);
    result.points.push_back(v);
  }
  for (auto& hole : shape.holes) {
    holes.push_back({});
    result.holes.push_back({});
    for (auto& v : hole) {
      const auto index = result.points.size();
      holes.back().push_back({.v={v.x, v.y, 0}, .i=index});
      result.holes.back().push_back(index);
      result.points.push_back(v);
    }
  }

  Builder builder{};


  GLUtesselator* tobj = gluNewTess();

  if (!tobj) {
    throw std::runtime_error("can not create tessellation object");
  }

  gluTessCallback(tobj, GLU_TESS_VERTEX_DATA, (_GLUfuncptr) &vertexCallback);
  gluTessCallback(tobj, GLU_TESS_ERROR_DATA, (_GLUfuncptr) &error);
  gluTessCallback(tobj, GLU_TESS_BEGIN_DATA, (_GLUfuncptr) &tess_begin);
  gluTessCallback(tobj, GLU_TESS_END_DATA, (_GLUfuncptr) &tess_end);

  gluTessBeginPolygon(tobj, &builder);
  for (auto& hole : holes) {
    gluTessBeginContour(tobj);
    for (auto& vertex : hole) {
      gluTessVertex(tobj, vertex.v, &vertex.i);
    }
    gluTessEndContour(tobj);
  }
  gluTessEndPolygon(tobj);

  gluDeleteTess(tobj);

  result.indices = builder.getTriangles();
  return result;
}


}
