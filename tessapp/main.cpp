#include "main.h"
#include "geometry/2d/ShapeWithHoles.h"
#include "output/Canvas.h"
#include "output/XmlWriter.h"
#include "tessellation/tessellator.h"
#include <GL/glu.h>
#include <array>
#include <fstream>
#include <iostream>
#include <vector>

namespace tessapp
{
namespace
{


const output::Style triangleStyle {
  .fill{{0x00, 0xe0, 0x00}},
  .stroke{0.1, {0,0,0}},
};

const output::Style shapeStyle {
  .fill{{0x80, 0x80, 0x80}},
  .stroke{0.1, {0,0,0}},
};


}


void main()
{
  geometry::_2d::ShapeWithHoles shape{
    .outline {
      {  0,  0 },
      {  1,  5 },
      {  0, 10 },
      { 10, 10 },
      { 15,  5 },
      { 10,  0 },
    },
    .holes {
      {
        {  2,  2 },
        {  5,  5 },
        {  8,  2 },
        {  8,  8 },
        {  2,  8 },
      },
    },
  };

  const auto triangles = tessellation::tessellate(shape);

  std::vector<output::Entry> shapes{};

  geometry::_2d::ShapeWithHoles nh{};

  for (const auto& oi : triangles.outline) {
    nh.outline.push_back(triangles.points.at(oi));
  }

  for (const auto& hole : triangles.holes) {
    nh.holes.push_back({});
    for (const auto& hi : hole) {
      nh.holes.back().push_back(triangles.points.at(hi));
    }
  }

  shapes.push_back(output::Entry{nh, shapeStyle});

  for (const auto& triangle : triangles.indices) {
    const geometry::_2d::Shape outline{
      triangles.points.at(triangle[0]),
      triangles.points.at(triangle[1]),
      triangles.points.at(triangle[2]),
    };
    shapes.push_back(output::Entry{{.outline=outline}, triangleStyle});
  }

  std::ofstream svg{"tessellation.svg"};
  output::XmlWriter xmlWriter{svg};
  output::SvgWriter svgWriter{xmlWriter};
  printShapes(shapes, 5, svgWriter);
}


}
