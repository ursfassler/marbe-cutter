#pragma once

#include "geometry/2d/IndexedShapeWithHoles.h"
#include "geometry/3d/Point.h"
#include "model/ModelBuilder.h"
#include "output/XmlWriter.h"
#include <map>

namespace output
{


class X3dBuilder :
    public model::ModelBuilder
{
public:
  X3dBuilder(
    output::XmlWriter& out_
  );

  void beginDocument(const std::string& name, const model::Color& background) override;
  void endDocument() override;

  void extrude(const geometry::_2d::IndexedShapeWithHoles&, double deepth, const model::Color&) override;
  void cube(const geometry::_3d::Point& size, const model::Color&) override;
  void cylinder(double radius, double height, const model::Color&) override;
  void sphere(double radius, const model::Color&) override;

  void beginTranslate(const geometry::_3d::Point&) override;
  void endTranslate() override;
  void beginRotation(double, model::Dimension) override;
  void endRotation() override;

private:
  output::XmlWriter& out;
  void addAppearance(const model::Color&);

};


}
