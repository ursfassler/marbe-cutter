#include "X3dBuilder.h"
#include <fstream>

namespace output
{
namespace
{


template<typename T>
std::string toString(const std::vector<T>& data)
{
  std::string result{};

  bool first = true;
  for (const auto& value : data) {
    if (!first) {
      result += " ";
    }
    first = false;
    result += std::to_string(value);
  }

  return result;
}

geometry::_3d::Point graphicsCoord(const geometry::_3d::Point& p)
{
  return {-p.x, p.z, p.y};
}

geometry::_3d::Point graphicsSize(const geometry::_3d::Point& p)
{
  return {p.x, p.z, p.y};
}

std::string toString(const geometry::_3d::Point& data)
{
  return toString<double>({data.x, data.y, data.z});
}

std::string toString(const model::Color& data)
{
  return toString<double>({data.red / 255.0, data.green / 255.0, data.blue / 255.0});
}

std::string toString(bool value)
{
  return value ? "true" : "false";
}

void addTriangles(const geometry::_2d::TrianglesIndices& triangles, const std::size_t b, bool reverse, std::vector<int>& coordIndex)
{
  for (const auto& triangle : triangles) {
    const std::array<std::size_t, 3> v{
      b+triangle[0],
      b+triangle[1],
      b+triangle[2],
    };

    if (reverse) {
      coordIndex.insert(coordIndex.end(), v.rbegin(), v.rend());
    } else {
      coordIndex.insert(coordIndex.end(), v.begin(), v.end());
    }

    coordIndex.push_back(-1);
  }
}

void addSides(const std::vector<std::size_t>& outline, const std::size_t b, bool reverse, std::vector<int>& coordIndex)
{
  const std::size_t f = 0;
  const auto count = outline.size();
  for (std::size_t i = 0; i < count; i++) {
    const auto h0 = outline.at(i);
    const auto h1 = outline.at((i + 1) % count);
    const std::array<std::size_t, 4> v{
      f + h1,
      f + h0,
      b + h0,
      b + h1,
    };

    if (reverse) {
      coordIndex.insert(coordIndex.end(), v.rbegin(), v.rend());
    } else {
      coordIndex.insert(coordIndex.end(), v.begin(), v.end());
    }

    coordIndex.push_back(-1);
  }
}


}


X3dBuilder::X3dBuilder(output::XmlWriter& out_) :
  out{out_}
{
}

void X3dBuilder::beginDocument(const std::string& name, const model::Color& background)
{
  out.beginDocument("X3D PUBLIC \"ISO//Web3D//DTD X3D 3.0//EN\" \"https://www.web3d.org/specifications/x3d-3.0.dtd\"");

  out.beginNode("X3D");
  out.attribute("profile", "Interchange");
  out.attribute("version", "3.0");
  out.attribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema-instance");
  out.attribute("xsd:noNamespaceSchemaLocation", "https://www.web3d.org/specifications/x3d-3.0.xsd");

  out.beginNode("Scene");

  out.beginNode("WorldInfo");
  out.attribute("title", name);
  out.endNode();

  out.beginNode("Background");
  out.attribute("skyColor", toString(background));
  out.endNode();

  out.beginNode("NavigationInfo");
  out.attribute("headlight", toString(false));
  out.endNode();

  const std::vector<geometry::_3d::Point> lights{
    {200, 200, 250},
    {200, -200, 250},
    {-200, 200, 250},
    {-200, -200, 250},
  };
  const auto lightRadius = 500;

  for (const auto& light : lights) {
    out.beginNode("PointLight");
    out.attribute("on", toString(true));
    out.attribute("intensity", "0.75");
    out.attribute("radius", std::to_string(lightRadius));
    out.attribute("location", toString(graphicsCoord(light)));
    out.endNode();
  }
}

void X3dBuilder::endDocument()
{
  out.endNode();
  out.endNode();
  out.endDocument();
}

void X3dBuilder::extrude(const geometry::_2d::IndexedShapeWithHoles& shape, double deepth, const model::Color& color)
{
  out.beginNode("Shape");
  addAppearance(color);

  out.beginNode("IndexedFaceSet");
  out.attribute("ccw", toString(false));

  std::vector<int> coordIndex{};
  const auto b = shape.points.size();

  const bool reverse = deepth > 0;

  addTriangles(shape.indices, 0, reverse, coordIndex);
  addTriangles(shape.indices, b, !reverse, coordIndex);
  addSides(shape.outline, b, reverse, coordIndex);
  for (const auto& hole : shape.holes) {
    addSides(hole, b, reverse, coordIndex);
  }
  out.attribute("coordIndex", toString(coordIndex));

  out.beginNode("Coordinate");
  std::vector<double> points{};
  for (const auto& point : shape.points) {
    const auto p = graphicsCoord({point.x, point.y, 0});
    points.push_back(p.x);
    points.push_back(p.y);
    points.push_back(p.z);
  }
  for (const auto& point : shape.points) {
    const auto p = graphicsCoord({point.x, point.y, deepth});
    points.push_back(p.x);
    points.push_back(p.y);
    points.push_back(p.z);
  }
  out.attribute("point", toString(points));
  out.endNode();

  out.endNode();

  out.endNode();
}

void X3dBuilder::cube(const geometry::_3d::Point& size, const model::Color& color)
{
  out.beginNode("Shape");
  addAppearance(color);

  out.beginNode("Box");
  out.attribute("size", toString(graphicsSize(size)));
  out.endNode();

  out.endNode();
}

void X3dBuilder::cylinder(double radius, double height, const model::Color& color)
{
  out.beginNode("Shape");
  addAppearance(color);

  out.beginNode("Cylinder");
  out.attribute("radius", std::to_string(radius));
  out.attribute("height", std::to_string(height));
  out.attribute("top", toString(true));
  out.endNode();

  out.endNode();
}

void X3dBuilder::sphere(double radius, const model::Color& color)
{
  out.beginNode("Shape");
  addAppearance(color);

  out.beginNode("Sphere");
  out.attribute("radius", std::to_string(radius));
  out.endNode();

  out.endNode();
}

void X3dBuilder::beginTranslate(const geometry::_3d::Point& value)
{
  out.beginNode("Transform");
  out.attribute("translation", toString(graphicsCoord(value)));
}

void X3dBuilder::endTranslate()
{
  out.endNode();
}

void X3dBuilder::beginRotation(double value, model::Dimension dimension)
{
  const auto dir = graphicsCoord(
        {
          dimension == model::Dimension::X ? 1.0 : 0.0,
          dimension == model::Dimension::Y ? 1.0 : 0.0,
          dimension == model::Dimension::Z ? 1.0 : 0.0,
        });

  out.beginNode("Transform");
  out.attribute("rotation", toString<double>({dir.x, dir.y, dir.z, value}));
}

void X3dBuilder::endRotation()
{
  out.endNode();
}

void X3dBuilder::addAppearance(const model::Color& color)
{
  out.beginNode("Appearance");
  out.beginNode("Material");
  out.attribute("diffuseColor", toString(color));
  out.endNode();
  out.endNode();
}


}
