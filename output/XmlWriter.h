#pragma once

#include <optional>
#include <ostream>
#include <vector>

namespace output
{


class XmlWriter
{
public:
  XmlWriter(
    std::ostream& out_
  );

  void beginDocument(const std::optional<std::string>& doctype = {});
  void endDocument();
  void beginNode(const std::string& name);
  void endNode();
  void attribute(const std::string& name, const std::string& value);

private:
  std::ostream& out;
  std::vector<std::string> nodes{};

  bool nodeIsOpen{false};
  void closeNode();
  std::string indent(std::size_t level) const;

};


}
