#include "Canvas.h"
#include "appearance/colors.h"
#include <gtest/gtest.h>


namespace output::test
{


class CanvasTest :
    public ::testing::Test
{
protected:
  Canvas canvas{3};

  std::string getSvg() const
  {
    std::stringstream ss{};
    output::XmlWriter xml{ss};
    output::SvgWriter svg{xml};
    canvas.printSvg(svg);
    return ss.str();
  }
};


TEST_F(CanvasTest, closed_area)
{
  geometry::_2d::Shape shape{{0,0}, {4,7}, {5,2}};
  Area style{.fill = appearance::Gray, .stroke = {.width = 0.5, .color = appearance::Red}};
  const auto expected = R"f(<?xml version="1.0" encoding="UTF-8"?>
<svg
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    width="11mm"
    height="13mm"
    viewBox="-3 -10 11 13">
  <g
      transform="scale(1,-1)">
    <path
        fill="#7f7f7f"
        stroke="#ff0000"
        stroke-width="0.500000"
        d="M0,0 L4,7 L5,2 Z"/>
  </g>
</svg>
)f";

  canvas.addArea(shape, style, 1);

  ASSERT_EQ(expected, getSvg());
}

TEST_F(CanvasTest, closed_area_3_times)
{
  geometry::_2d::Shape shape{{0,0}, {2,0}, {1,3}};
  Area style{.fill = appearance::Gray, .stroke = {.width = 0.5, .color = appearance::Red}};
  const auto expected = R"f(<?xml version="1.0" encoding="UTF-8"?>
<svg
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    width="18mm"
    height="9mm"
    viewBox="-3 -6 18 9">
  <g
      transform="scale(1,-1)">
    <path
        fill="#7f7f7f"
        stroke="#ff0000"
        stroke-width="0.500000"
        d="M0,0 L2,0 L1,3 Z"/>
    <path
        fill="#7f7f7f"
        stroke="#ff0000"
        stroke-width="0.500000"
        d="M5,0 L7,0 L6,3 Z"/>
    <path
        fill="#7f7f7f"
        stroke="#ff0000"
        stroke-width="0.500000"
        d="M10,0 L12,0 L11,3 Z"/>
  </g>
</svg>
)f";

  canvas.addArea(shape, style, 3);

  ASSERT_EQ(expected, getSvg());
}

TEST_F(CanvasTest, open_path)
{
  geometry::_2d::Shape shape{{0,0}, {3,5}};
  Stroke style{.width = 2, .color = appearance::Black};
  const auto expected = R"f(<?xml version="1.0" encoding="UTF-8"?>
<svg
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    width="9mm"
    height="11mm"
    viewBox="-3 -8 9 11">
  <g
      transform="scale(1,-1)">
    <path
        fill="none"
        stroke="#000000"
        stroke-width="2"
        d="M0,0 L3,5"/>
  </g>
</svg>
)f";

  canvas.addPath(shape, style );

  ASSERT_EQ(expected, getSvg());
}


}
