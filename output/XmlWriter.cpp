#include "XmlWriter.h"

namespace output
{


XmlWriter::XmlWriter(
    std::ostream& out_
    ) :
  out{out_}
{
}

void XmlWriter::beginDocument(const std::optional<std::string>& doctype)
{
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
  if (doctype) {
    out << "<!DOCTYPE " + *doctype + ">" << std::endl;
  }
}

void XmlWriter::endDocument()
{
}

void XmlWriter::beginNode(const std::string& name)
{
  closeNode();
  out << indent(nodes.size()) << "<" << name;
  nodes.push_back(name);
  nodeIsOpen = true;
}

void XmlWriter::endNode()
{
  if (nodeIsOpen) {
    out << "/>" << std::endl;
    nodes.pop_back();
    nodeIsOpen = false;
  } else {
    closeNode();
    const auto name = nodes.back();
    nodes.pop_back();
    out << indent(nodes.size()) << "</" << name + ">" << std::endl;
  }
}

void XmlWriter::closeNode()
{
  if (nodeIsOpen) {
    out << ">" << std::endl;
    nodeIsOpen = false;
  }
}

void XmlWriter::attribute(const std::string& name, const std::string& value)
{
  out << std::endl;
  out << indent(nodes.size()+1) << name << "=\"" << value << "\"";
}

std::string XmlWriter::indent(std::size_t level) const
{
  return std::string(2 * level, ' ');
}


}
