#pragma once

#include "model/Color.h"
#include "geometry/2d/Point.h"
#include "geometry/2d/Shape.h"
#include "geometry/2d/ShapeWithHoles.h"
#include "output/SvgWriter.h"
#include <optional>
#include <vector>

namespace output
{


struct Area {
  model::Color fill{};
  Stroke stroke{};
};

struct Entry {
  geometry::_2d::ShapeWithHoles shape;
  Style style;
};


class Canvas
{
public:
  Canvas(double spacing);

  void addPath(const geometry::_2d::Shape&, const Stroke&);
  void addArea(const geometry::_2d::Shape&, const Area&, std::size_t count);
  void addArea(const geometry::_2d::ShapeWithHoles&, const Area&, std::size_t count);
  void printSvg(SvgWriter&) const;

private:
  double spacing;
  geometry::_2d::Point offset{0, 0};
  std::vector<Entry> shapes{};

  void add(const geometry::_2d::ShapeWithHoles&, const Style&, std::size_t count);
};


void printShapes(const std::vector<Entry>&, double spacing, SvgWriter&);


}
