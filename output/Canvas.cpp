#include "Canvas.h"

namespace output
{
namespace
{


geometry::_2d::Point minPoint(const geometry::_2d::Shape& value)
{
  geometry::_2d::Point result{
    .x = value.at(0).x,
    .y = value.at(0).y,
  };

  for (const auto& point : value) {
    result.x = std::min(result.x, point.x);
    result.y = std::min(result.y, point.y);
  }

  return result;
}

geometry::_2d::Point dimension(const geometry::_2d::Shape& value)
{
  double xMin = 0;
  double xMax = 0;
  double yMin = 0;
  double yMax = 0;
  for (const auto& point : value) {
    xMin = std::min(xMin, point.x);
    xMax = std::max(xMax, point.x);
    yMin = std::min(yMin, point.y);
    yMax = std::max(yMax, point.y);
  }

  return {
    .x = xMax - xMin,
    .y = yMax - yMin,
  };
}


}


Canvas::Canvas(double spacing_) :
  spacing{spacing_}
{
}

void Canvas::addPath(const geometry::_2d::Shape& shape, const Stroke& stroke)
{
  add({.outline=shape}, {.fill = {}, .stroke = stroke}, 1);
}

void Canvas::addArea(const geometry::_2d::Shape& shape, const Area& area, std::size_t count)
{
  add({.outline=shape}, {.fill = area.fill, .stroke = area.stroke}, count);
}

void Canvas::addArea(const geometry::_2d::ShapeWithHoles& shape, const Area& area, std::size_t count)
{
  add(shape, {.fill = area.fill, .stroke = area.stroke}, count);
}

void Canvas::add(const geometry::_2d::ShapeWithHoles& shape, const Style& style, std::size_t count)
{
  const auto min = minPoint(shape.outline);
  const auto belowExisting = offset - min;

  const auto shapeSize = output::dimension(shape.outline);
  const geometry::_2d::Vector rightDelta{.x=shapeSize.x + spacing, .y=0};

  auto placed = geometry::_2d::moveBy(shape, belowExisting);

  for (std::size_t i = 0; i < count; i++) {
    shapes.push_back({placed, style});
    placed = geometry::_2d::moveBy(placed, rightDelta);
  }

  offset = offset + geometry::_2d::Vector{0, shapeSize.y + spacing};
}

void Canvas::printSvg(SvgWriter& out) const
{
  printShapes(shapes, spacing, out);
}



void printShapes(const std::vector<Entry>& shapes, double spacing, SvgWriter& out)
{
  geometry::_2d::Point min{};
  geometry::_2d::Point max{};
  for (const auto& shape : shapes) {
    for (const auto& point : shape.shape.outline) {
      min.x = std::min(min.x, point.x);
      max.x = std::max(max.x, point.x);
      min.y = std::min(min.y, point.y);
      max.y = std::max(max.y, point.y);
    }
  }

  min.x -= spacing;
  min.y -= spacing;
  max.x += spacing;
  max.y += spacing;

  out.beginDocument(min, max);

  for (const auto& shape : shapes) {
    out.path(shape.shape, shape.style);
  }

  out.endDocument();
}


}
