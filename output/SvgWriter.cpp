#include "SvgWriter.h"
#include <algorithm>
#include <cmath>

namespace output
{
namespace
{


std::string hex(uint8_t value)
{
  std::string result{};

  for (int i = 0; i < 2; i++) {
    const uint8_t nibble = value & 0xf;
    const char base = (nibble < 10) ? '0' : ('a' - 10);
    const char sym = nibble + base;
    result += sym;
    value = value >> 4;
  }

  std::reverse(result.begin(), result.end());

  return result;
}

std::string colorString(const std::optional<model::Color>& value)
{
  if (!value) {
    return "none";
  }

  std::string result{};
  result += "#";
  result += hex(value->red);
  result += hex(value->green);
  result += hex(value->blue);

  return result;
}

std::string toString(double value)
{
  //TODO find cleaner way
  const int asInt = std::round(value);
  const double delta = asInt - value;
  if (delta == 0) {
    return std::to_string(asInt);
  } else {
    return std::to_string(value);
  }
}

std::string createPath(const geometry::_2d::Shape& shape, bool fill)
{
  std::string path{};

  bool first = true;
  for (const auto& point : shape) {
    std::string command = first ? "M" : "L";
    std::string separator = first ? "" : " ";
    path += separator + command + toString(point.x) + "," + toString(point.y);
    first = false;
  }
  if (fill) {
    path += " Z";
  }

  return path;
}


}


SvgWriter::SvgWriter(XmlWriter& xml_) :
  xml{xml_}
{}

void SvgWriter::beginDocument(const geometry::_2d::Point& min, const geometry::_2d::Point& max)
{
  const auto size = max - min;

  xml.beginDocument();

  xml.beginNode("svg");
  xml.attribute("version", "1.1");
  xml.attribute("xmlns", "http://www.w3.org/2000/svg");
  xml.attribute("width", toString(size.x) + "mm");
  xml.attribute("height", toString(size.y) + "mm");
  xml.attribute("viewBox",
                toString(min.x) + " " +
                toString(-max.y) + " " +
                toString(size.x) + " " +
                toString(size.y));

  xml.beginNode("g");
  xml.attribute("transform", "scale(1,-1)");
}

void SvgWriter::endDocument()
{
  xml.endNode();
  xml.endNode();
  xml.endDocument();
}

void SvgWriter::path(const geometry::_2d::ShapeWithHoles& shape, const Style& style)
{
  std::string path{};
  path += createPath(shape.outline, style.fill.has_value());
  for (const auto& hole : shape.holes) {
    path += " " + createPath(hole, style.fill.has_value());
  }

  xml.beginNode("path");
  xml.attribute("fill", colorString(style.fill));
  xml.attribute("stroke", colorString(style.stroke.color));
  xml.attribute("stroke-width", toString(style.stroke.width));
  xml.attribute("d", path);
  xml.endNode();
}


}
