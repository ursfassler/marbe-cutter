#pragma once

#include "XmlWriter.h"
#include "geometry/2d/Point.h"
#include "geometry/2d/ShapeWithHoles.h"
#include "model/Color.h"

namespace output
{

struct Stroke {
  double width{};
  model::Color color{};
};

struct Style {
  std::optional<model::Color> fill{};
  Stroke stroke{};
};


class SvgWriter
{
public:
  SvgWriter(XmlWriter&);

  void beginDocument(const geometry::_2d::Point& min, const geometry::_2d::Point& max);
  void endDocument();

  void path(const geometry::_2d::ShapeWithHoles&, const Style&);

private:
  XmlWriter& xml;
};


}
